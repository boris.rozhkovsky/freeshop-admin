package com.freeshop.admin.utils;

import com.freeshop.admin.BuildConfig;

import java.io.PrintWriter;
import java.io.StringWriter;

public final class Log {

	public static final String DEFAULT_TAG = "FRESSHOP";
	public static final boolean LOG_ENABLED = BuildConfig.LOG_ENABLED;

	
	public static void logLongText(String text) {
		if (LOG_ENABLED) {
			v("=========");
			while (text.length() > 0) {
				if (text.length() > 1000) {
					String firstSymbols = text.substring(0, 1000);
					text = text.substring(1000, text.length());
					v(firstSymbols);
				} else {
					v(text);
					text = "";
				}
			}
			v("=========");
		}
	}
	
	private static String safeTrim(String str) {
		if (str == null)
			return "";
		if (str.length() > 3000) {
			return str.substring(0, 3000);
		}
		return str;
	}

	public static final void i(String tag, String string) {
		string = safeTrim(string);
		if (LOG_ENABLED)
			android.util.Log.i(tag, string);
	}
	public static final void e(String tag, String string) {
		string = safeTrim(string);
		if (LOG_ENABLED)
			android.util.Log.e(tag, string);
	}
	public static final void d(String tag, String string) {
		string = safeTrim(string);
		if (LOG_ENABLED)
			android.util.Log.d(tag, string);
	}
	public static final void v(String tag, String string) {
		string = safeTrim(string);
		if (LOG_ENABLED)
			android.util.Log.v(tag, string);
	}
	public static final void w(String tag, String string) {
		string = safeTrim(string);
		if (LOG_ENABLED)
			android.util.Log.w(tag, string);
	}
	public static final void i(String string) {
		if (LOG_ENABLED) i(DEFAULT_TAG, string);
	}
	public static final void e(String string) {
		if (LOG_ENABLED) e(DEFAULT_TAG, string);
	}
	public static final void d(String string) {
		if (LOG_ENABLED) d(DEFAULT_TAG, string);
	}
	public static final void v(String string) {
		if (LOG_ENABLED) v(DEFAULT_TAG, string);
	}
	public static final void w(String string) {
		if (LOG_ENABLED) w(DEFAULT_TAG, string);
	}
	
	private static String throwableToString(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}
	
	public static final void i(String string, Throwable t) {
		if (LOG_ENABLED) { 
			i(DEFAULT_TAG, string + "\n" + throwableToString(t));
		}
	}
	public static final void e(String string, Throwable t) {
		if (LOG_ENABLED) {
			e(DEFAULT_TAG, string + "\n" + throwableToString(t));
		}
	}
	public static final void d(String string, Throwable t) {
		if (LOG_ENABLED) {
			d(DEFAULT_TAG, string + "\n" + throwableToString(t));
		}
	}
	public static final void v(String string, Throwable t) {
		if (LOG_ENABLED) {
			v(DEFAULT_TAG, string + "\n" + throwableToString(t));
		}
	}
	public static final void w(String string, Throwable t) {
		if (LOG_ENABLED) { 
			w(DEFAULT_TAG, string + "\n" + throwableToString(t));
		}
	}

}