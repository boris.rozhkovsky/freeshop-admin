package com.freeshop.admin.utils;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    private static Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    private static Point screenSize;
    private static int miniumWidth;

    public static byte[] serialize(Serializable object) {
        try {
            byte[] result;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(object);
            result = bos.toByteArray();
            out.close();
            bos.close();
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> T deserialize(byte[] bytes) {
        try {
            T result;
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInput in = new ObjectInputStream(bis);
            result = (T) in.readObject();
            bis.close();
            in.close();
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static int getDaysBetween(Date a, Date b) {
        return (int) TimeUnit.MILLISECONDS.toDays(a.getTime() - b.getTime());
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$");
        Matcher m = pattern.matcher(email);
        return m.find();
    }

    public static String join(String a, String b, String delimeter) {
        if (TextUtils.isEmpty(a) && TextUtils.isEmpty(b))
            return null;
        if (TextUtils.isEmpty(a))
            return b;
        if (TextUtils.isEmpty(b))
            return a;
        return a + delimeter + b;
    }

    public static void showKeyboard(final View view) {
        mainThreadHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            }
        }, 100);
    }

    public static void hideKeyboard(final View view) {
        mainThreadHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }, 100);
    }

    public static String capitalize(String input) {
        if (TextUtils.isEmpty(input) == false) {
            input = input.toLowerCase(Locale.US);
            input = Character.toUpperCase(input.charAt(0)) + input.substring(1);
        }
        return input;
    }


    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static Handler getMainThreadHandler() {
        return mainThreadHandler;
    }

}
