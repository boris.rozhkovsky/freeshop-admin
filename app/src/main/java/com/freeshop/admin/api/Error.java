package com.freeshop.admin.api;

public enum Error {

    NO_ERROR(0),
    UNKNOWN_ERROR(0),
    SERVICE_ERROR(0),
    NO_CONNECTION(0);

    private int code;
    public int getCode(){
        return code;
    }

    private Error(int code) {
        this.code = code;
    }

    static Error fromCode(int code){
        for (Error er : Error.values()){
            if (er.getCode()==code){
                return er;
            }
        }
        return UNKNOWN_ERROR;
    }
}
