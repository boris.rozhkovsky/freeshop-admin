package com.freeshop.admin.api.tasks;

import android.content.Context;

import com.freeshop.admin.R;
import com.freeshop.admin.model.authorization.AuthorizationManager;
import com.freeshop.admin.model.vk.Post;
import com.freeshop.admin.model.vk.attachments.Attachment;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PublishNewPostTask extends VkAsyncTask {

    private boolean isSigned;
    private List<Attachment> attachments;
    private long ownerId;
    private String message;

    public PublishNewPostTask(Context context, long ownerId, List<Attachment> attachments, String message, boolean isSigned, VkAsyncTaskListener vkAsyncTaskListener) {
        super(context, vkAsyncTaskListener);
        this.isSigned = isSigned;
        this.ownerId = ownerId;
        this.attachments = attachments;
        this.message = message;
    }

    @Override
    protected VKRequest getRequest() {

        Map<String, Object> params = new HashMap<>();
        params.put("owner_id", ownerId);

        if (message != null) {
            params.put("message", message);
        }

        if (attachments != null && attachments.size() != 0) {
            params.put("attachments", Post.getAttachmentsString(attachments));
        }

        params.put("signed", isSigned ? "1" : "0");
        params.put("access_token", AuthorizationManager.getInstance().getAccessToken().accessToken);

        return new VKRequest("wall.post", new VKParameters(params));
    }

    @Override
    protected String getDialogMessage() {
        return context.getResources().getString(R.string.publishing_post);
    }
}
