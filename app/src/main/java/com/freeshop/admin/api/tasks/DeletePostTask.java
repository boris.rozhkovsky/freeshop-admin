package com.freeshop.admin.api.tasks;

import android.content.Context;

import com.freeshop.admin.R;
import com.freeshop.admin.model.vk.Post;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

public class DeletePostTask extends VkAsyncTask {

    private Post post;

    public DeletePostTask(Context context, Post post, VkAsyncTaskListener vkAsyncTaskListener) {
        super(context, vkAsyncTaskListener);
        this.post = post;
    }

    @Override
    protected VKRequest getRequest() {

        long ownerId = getAuthorIdForRequest(post.getOwner());

        return new VKRequest("wall.delete",
                VKParameters.from("owner_id", ownerId,
                        "post_id", post.getId()));
    }

    @Override
    protected String getDialogMessage() {
        return context.getResources().getString(R.string.deleting_post);
    }
}
