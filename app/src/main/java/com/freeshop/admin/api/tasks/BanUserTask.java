package com.freeshop.admin.api.tasks;

import android.content.Context;
import android.text.TextUtils;

import com.freeshop.admin.Config;
import com.freeshop.admin.R;
import com.freeshop.admin.model.vk.User;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.HashMap;
import java.util.Map;

public class BanUserTask extends VkAsyncTask {

    private User user;
    private boolean showComment;
    private boolean constantBan;
    private String comment;
    private int reason;
    private LocalDate until;

    public BanUserTask(Context context, User user, String comment, LocalDate until, int reason, boolean showComment, boolean constantBan, VkAsyncTaskListener vkAsyncTaskListener) {
        super(context, vkAsyncTaskListener);
        this.user = user;
        this.comment = comment;
        this.until = until;
        this.reason = reason;
        this.showComment = showComment;
        this.constantBan = constantBan;
    }


    @Override
    protected VKRequest getRequest() {
        Map<String, Object> params = new HashMap<>();
        params.put("user_id", user.getId());
        params.put("group_id", Math.abs(Config.currentGroup.getGroupId()));

        if (!TextUtils.isEmpty(comment)) {
            params.put("comment", comment);
        }

        params.put("comment_visible", showComment ? "1" : "0");

        params.put("reason", reason);
        if (!constantBan) {
            params.put("end_date", Math.round(until.toDateTime(new LocalTime()).getMillis()/1000));
        }

        return new VKRequest("groups.banUser", new VKParameters(params));
    }

    @Override
    protected String getDialogMessage() {
        return context.getResources().getString(R.string.ban_user);
    }
}
