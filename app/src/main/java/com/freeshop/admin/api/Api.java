package com.freeshop.admin.api;

import android.os.AsyncTask;
import android.util.Log;

import com.freeshop.admin.utils.InputStreamUtils;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Api {

    public static final int DEFAULT_CONNECTION_TIMEOUT = 15000;
    public static final int DEFAULT_READ_TIMEOUT = 60000;


    public interface OnRequestResultListener {
        void onRequestFinished(Response response);
    }

    public static void executeAsync(final Request request, final OnRequestResultListener listener) {
        new AsyncTask<Void, Void, Response>() {
            @Override
            protected Response doInBackground(Void... voids) {
                return Api.execute(request);
            }

            @Override
            protected void onPostExecute(Response response) {
                if (listener != null) {
                    listener.onRequestFinished(response);
                }
            }
        }.execute();
    }

    public static Response execute(Request request) {
        String url = request.getUrl();
        String paramsString = request.getParamsAsString();
        if (request.getHttpMethod() == HttpMethod.GET) {
            if (paramsString.length() > 0) {
                url += "?" + paramsString;
            }
            Log.d("api", "Executing request: curl " + url);
        } else {
            Log.d("api", "Executing request: curl -d \"" + request.getParamsAsString() + "\" " + url);
        }

        InputStream input = null;
        OutputStream ostream = null;
        try {

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            connection.setReadTimeout(DEFAULT_READ_TIMEOUT);
            connection.setDoInput(true);
            connection.setDoOutput(request.getHttpMethod() == HttpMethod.POST);
            connection.setRequestMethod(request.getHttpMethod().toString());

            if (request.getHttpMethod() == HttpMethod.POST) {
                if (request.getParts().size() == 0) {
                    ostream = new DataOutputStream(connection.getOutputStream());
                    ostream.write(request.getParamsAsString().getBytes("UTF-8"));
                }
            }

            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                String response = InputStreamUtils.toString(connection.getInputStream());
                Log.i("api","Server responded with status code " + responseCode + " response size: " + response.length());
              //  Log.logLongText(response);
                return new Response(responseCode, response);
            } else if (responseCode >= 500){
                Log.e("api","Request failed with status code 500");
                String response = InputStreamUtils.toString(connection.getInputStream());
                if (response != null) {
                 //   Log.logLongText(response);
                }
                return new Response(Error.SERVICE_ERROR);
            } else {
                Log.e("api","Request " + url + " failed with status code " + responseCode);
                return new Response(Error.UNKNOWN_ERROR);
            }
        } catch (Exception e) {
            Log.e("api","Failed to perform " + url + " request",e);
            return new Response(Error.UNKNOWN_ERROR);
        } finally {
            try {
                if (ostream != null)
                    ostream.close();
                if (input != null)
                    input.close();
            } catch (Exception ignore) { /* ignore */ }
        }
    }

}
