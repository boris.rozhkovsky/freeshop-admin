package com.freeshop.admin.api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Response {

    private boolean success;
    private int statusCode;
    private Error error;
    private JSONObject json;

    public Response(Error error) {
        this.error = error;
        success = error == Error.NO_ERROR;
    }

    public Response(int statusCode, String responseString) throws IOException, JSONException {
        this.statusCode = statusCode;
        Log.d("Response","Server responded with status " + statusCode);

        this.json = new JSONObject(responseString);
        this.success = statusCode == 200;
        if (!success) {
            error = Error.UNKNOWN_ERROR;
        } else {
            error = Error.NO_ERROR;
        }
    }

    public JSONObject getJson() {
        return json;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getStatusCode() {
        return statusCode;
    }


    public Error getError() {
        return error;
    }

}
