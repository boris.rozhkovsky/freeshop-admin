package com.freeshop.admin.api.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import com.freeshop.admin.BuildConfig;
import com.freeshop.admin.R;
import com.freeshop.admin.api.Api;
import com.freeshop.admin.api.HttpMethod;
import com.freeshop.admin.api.Request;
import com.freeshop.admin.api.Response;
import com.freeshop.admin.model.telegram.Bot;
import com.freeshop.admin.model.telegram.Channel;
import com.freeshop.admin.utils.ParseUtils;

import java.util.ArrayList;
import java.util.List;

public class SendToTelegramTask extends AsyncTask<Void, Void, Response> {

    private Channel channel;
    private Bot bot;
    private String message;
    protected ProgressDialog progressDialog;
    private Context context;
    private Boolean sendSilently;
    private TelegramTaskListener telegramTaskListener;

    public SendToTelegramTask(Context context, Bot bot, Channel channel, String message, boolean sendSilently, TelegramTaskListener telegramTaskListener) {
        this.channel = channel;
        this.bot = bot;
        this.message = message;
        this.context = context;
        this.sendSilently = sendSilently;
        this.telegramTaskListener = telegramTaskListener;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = ProgressDialog.show(context, context.getString(R.string.please_wait), context.getString(R.string.sending_to_telegram));
    }

    @Override
    protected Response doInBackground(Void... params) {
        StringBuilder pathBuilder = new StringBuilder();
        pathBuilder.append(BuildConfig.TELEGRAM_API_URL);
        pathBuilder.append(bot.getPath());
        pathBuilder.append("/sendMessage");

        String path = pathBuilder.toString();
        List<String> postParams = new ArrayList<>();

        postParams.add("chat_id");
        postParams.add("@" + channel.getPath());

        postParams.add("text");
        postParams.add(message);

        postParams.add("parse_mode");
        postParams.add("Markdown");

        postParams.add("disable_notification");
        postParams.add(sendSilently ? "true" : "false");

        Request request = new Request(HttpMethod.POST, path, null, postParams);
        return Api.execute(request);
    }

    @Override
    protected void onPostExecute(Response result) {
        progressDialog.dismiss();
        boolean isSuccess = result.isSuccess() && ParseUtils.objToBoolean(result.getJson().optBoolean("ok", false));
        if (!isSuccess) {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.error)
                    .setMessage(context.getResources().getString(R.string.error_while_send_to_telegram))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
            if (telegramTaskListener != null) {
                telegramTaskListener.onError();
            }
        } else {
            if (telegramTaskListener != null) {
                telegramTaskListener.onSuccess(null);
            }
        }
    }

    public interface TelegramTaskListener {
        void onSuccess(Object data);

        void onError();
    }


}
