package com.freeshop.admin.api;

public enum HttpMethod {
    POST,
    GET
}
