package com.freeshop.admin.api.tasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.freeshop.admin.Config;
import com.freeshop.admin.R;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class UploadPhotoTask extends VkAsyncTask {

    private File file;
    private Uri uri;

    public UploadPhotoTask(Context context) {
        super(context);
    }

    public UploadPhotoTask(Context context, File file, Uri uri, VkAsyncTaskListener vkAsyncTaskListener) {
        super(context, vkAsyncTaskListener);

        this.file = file;
        this.uri = uri;
    }

    @Override
    protected VKRequest getRequest() {
        InputStream imageStream = null;
        try {
            imageStream = context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);

        return VKApi.uploadWallPhotoRequest(new VKUploadImage(yourSelectedImage, VKImageParameters.jpgImage(0.9f)), 0, Math.abs((int) Config.currentGroup.getGroupId()));
    }

    @Override
    public void onComplete(VKResponse response) {
        progressDialog.hide();
        if (vkAsyncTaskListener != null) {
            vkAsyncTaskListener.onSuccess(response.parsedModel);
        }
    }

    @Override
    protected String getDialogMessage() {
        return context.getResources().getString(R.string.uploading_photo);
    }

}
