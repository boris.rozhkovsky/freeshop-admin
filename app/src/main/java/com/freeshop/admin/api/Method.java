package com.freeshop.admin.api;

import com.freeshop.admin.BuildConfig;

public enum Method {

    TELEGRAM_SEND_MESSAGE(HttpMethod.POST, BuildConfig.TELEGRAM_API_URL, "sendMessage");

    private final String path;
    private final String apiPath;
    private final HttpMethod httpMethod;

    private Method(HttpMethod httpMethod, String apiPath, String path) {
        this.apiPath = apiPath;
        this.path = path;
        this.httpMethod = httpMethod;
    }

    public String getPath() {
        return  String.format("%s/%s",apiPath,path);
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

}
