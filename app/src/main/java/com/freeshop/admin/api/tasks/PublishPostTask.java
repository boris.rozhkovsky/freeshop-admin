package com.freeshop.admin.api.tasks;

import android.content.Context;

import com.freeshop.admin.R;
import com.freeshop.admin.model.authorization.AuthorizationManager;
import com.freeshop.admin.model.vk.Post;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

import java.util.HashMap;
import java.util.Map;

public class PublishPostTask extends VkAsyncTask {

    private Post post;
    private boolean isSigned;

    public PublishPostTask(Context context, Post post, boolean isSigned, VkAsyncTaskListener vkAsyncTaskListener) {
        super(context, vkAsyncTaskListener);
        this.post = post;
        this.isSigned = isSigned;
    }

    @Override
    protected VKRequest getRequest() {
        long ownerId = getAuthorIdForRequest(post.getOwner());
        return new VKRequest("wall.post",
                VKParameters.from("owner_id", ownerId,
                        "post_id", post.getId(),
                        "message", post.getText(),
                        "signed", isSigned ? "1" : "0",
                        "attachments", post.getAttachmentsString(),
                        "access_token", AuthorizationManager.getInstance().getAccessToken().accessToken));
    }

    @Override
    protected String getDialogMessage() {
        return context.getResources().getString(R.string.publishing_post);
    }
}
