package com.freeshop.admin.api.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.freeshop.admin.R;
import com.freeshop.admin.model.vk.Author;
import com.freeshop.admin.model.vk.Group;
import com.freeshop.admin.model.vk.User;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

public abstract class VkAsyncTask extends VKRequest.VKRequestListener {

    protected Context context;
    protected VkAsyncTaskListener vkAsyncTaskListener;
    protected ProgressDialog progressDialog;


    public VkAsyncTask(Context context, VkAsyncTaskListener vkAsyncTaskListener) {
        this.context = context;
        this.vkAsyncTaskListener = vkAsyncTaskListener;
    }

    public VkAsyncTask(Context context) {
        this.context = context;
    }

    public void execute() {
        progressDialog = ProgressDialog.show(context, context.getString(R.string.please_wait), getDialogMessage());
        VKRequest request = getRequest();
        request.executeWithListener(this);
    }

    protected abstract VKRequest getRequest();

    protected static long getAuthorIdForRequest(Author owner) {
        int mathSign = 1;
        if (owner instanceof Group) {
            mathSign = -1;
        } else if (owner instanceof User) {
            mathSign = 1;
        }

        return mathSign * Math.abs(owner.getId());
    }

    @Override
    public void onComplete(VKResponse response) {
        super.onComplete(response);
        progressDialog.hide();
        if (vkAsyncTaskListener != null) {
            vkAsyncTaskListener.onSuccess(null);
        }
    }

    @Override
    public void onError(VKError error) {
        super.onError(error);
        progressDialog.hide();
        if (vkAsyncTaskListener != null) {
            vkAsyncTaskListener.onError();
        }

        String message = error.apiError != null ? error.apiError.errorMessage : context.getResources().getString(R.string.unknown_error);

        new AlertDialog.Builder(context)
                .setTitle(R.string.error)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    protected abstract String getDialogMessage();

    public interface VkAsyncTaskListener {
        void onSuccess(Object data);

        void onError();
    }

}
