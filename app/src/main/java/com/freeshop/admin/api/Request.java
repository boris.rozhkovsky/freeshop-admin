package com.freeshop.admin.api;

import android.graphics.Bitmap;

import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Request {

    private static String sizeToString(long sizeBytes) {
        if (sizeBytes < 1024 * 3) {
            return "(" + Long.toString(sizeBytes) + "B)";
        } else if (sizeBytes < 1024 * 1024 * 3) {
            return "(" + Long.toString(sizeBytes / 1024) + "KB)";
        } else {
            return "(" + Long.toString(sizeBytes / 1024 / 1024) + "MB)";
        }
    }

    public static abstract class PartDescription {

        protected final String name;

        protected PartDescription(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public abstract ContentBody toContentBody();

    }

    public static class FilePartDescription extends PartDescription {

        protected File file;
        protected String mimeType;

        public FilePartDescription(String name, File file, String mimeType) {
            super(name);
            this.file = file;
            this.mimeType = mimeType;
        }

        @Override
        public ContentBody toContentBody() {
            return new FileBody(file, mimeType);
        }

        @Override
        public String toString() {
            return "[" + name + " " + mimeType + " " + sizeToString(file.length()) + "]";
        }

    }

    public static class TextFilePartDescription extends FilePartDescription {

        public TextFilePartDescription(String name, File file) {
            super(name, file, "text/plain");
        }

        @Override
        public ContentBody toContentBody() {
            return new FileBody(file, mimeType, "UTF-8");
        }

    }

    public static class BlobPartDescription extends PartDescription {

        private byte[] data;
        private String mimeType;
        private String filename;

        public BlobPartDescription(String name, byte[] data, String mimeType, String filename) {
            super(name);
            this.data = data;
            this.mimeType = mimeType;
            this.filename = filename;
        }

        @Override
        public ContentBody toContentBody() {
            return new ByteArrayBody(data, mimeType, filename);
        }

        @Override
        public String toString() {
            return "[" + filename + " " + mimeType + " " + sizeToString(data.length) + "]";
        }

    }

    public static class BitmapPartDescription extends PartDescription {

        private byte[] data;

        public BitmapPartDescription(String name, Bitmap bitmap) {
            super(name);

            ByteArrayOutputStream imageOutput = null;
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                this.data = stream.toByteArray();
            } finally {
                try {
                    if (imageOutput != null) {
                        imageOutput.close();
                    }
                } catch (Exception ignore) {}
            }
        }

        @Override
        public ContentBody toContentBody() {
            return new ByteArrayBody(data, "image/jpg", name + ".jpeg");
        }

        @Override
        public String toString() {
            return "[" + name + ".jpeg " + sizeToString(data.length) + "]";
        }

    }

    private String path;
    private boolean pathIncludesDomain;
    private HttpMethod httpMethod;
    private List<String> urlParams;
    private HashMap<String, String> params;
    private List<PartDescription> parts;

    public Request(Method method, List<String> urlParams, List<String> params) {
        this(method.getHttpMethod(), method.getPath(), urlParams, params);
        pathIncludesDomain = false;
    }

    public Request(HttpMethod httpMethod, String path, List<String> urlParams, List<String> params) {
        this.path = path;
        this.pathIncludesDomain = true;
        this.httpMethod = httpMethod;
        this.urlParams = new ArrayList<>();
        this.params = new HashMap<>();
        this.parts = new ArrayList<>();
        if (urlParams != null) {
            addUrlParams(urlParams.toArray(new String[urlParams.size()]));
        }
        if (params != null) {
            addParams(params);
        }
    }

    public void addUrlParams(String... urlParams) {
        for (String param : urlParams) {
            this.urlParams.add(param);
        }
    }

    public void addParam(String key, String value) {
        params.put(key, value);
    }

    public void addParams(List<String> keyValuePairs) {
        if (keyValuePairs.size() % 2 == 1) {
            throw new RuntimeException("Invalid parameters count: " + keyValuePairs.size());
        }

        for (int i = 0; i < keyValuePairs.size(); i += 2) {
            String key = keyValuePairs.get(i);
            String value = keyValuePairs.get(i + 1);
            params.put(key, value);
        }
    }

    public void addParams(HashMap<String, String> keyValuePairs) {
        params.putAll(keyValuePairs);
    }

    public void addPart(PartDescription part) {
        parts.add(part);
    }

    List<PartDescription> getParts() {
        return parts;
    }

    HttpMethod getHttpMethod() {
        return httpMethod;
    }

    String getUrl() {
        String url = String.format(Locale.US, path, urlParams.toArray(new Object[urlParams.size()]));
        return url;
    }

    HashMap<String, String> getParams() {
        return params;
    }

    String getParamsAsString() {
        StringBuilder builder = new StringBuilder();
        for (String key : params.keySet()) {
            if (builder.length() != 0)
                builder.append("&");
            builder.append(key);
            builder.append("=");
            try {
                builder.append(URLEncoder.encode(params.get(key), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return builder.toString();
    }

}
