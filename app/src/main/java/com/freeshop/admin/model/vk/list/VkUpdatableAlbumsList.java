package com.freeshop.admin.model.vk.list;

import com.freeshop.admin.model.UpdatableList;
import com.freeshop.admin.model.vk.attachments.Album;
import com.freeshop.admin.model.vk.attachments.PhotoSizes;
import com.freeshop.admin.utils.Log;
import com.freeshop.admin.utils.ParseUtils;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public abstract class VkUpdatableAlbumsList extends UpdatableList<Album> {

    private List<Album> albums = new ArrayList<Album>();

    @Override
    public List<Album> getItems() {
        return albums;
    }

    protected abstract VKRequest getVKRequest();

    @Override
    public void update() {
        reportUpdateStarted();
        VKRequest request = getVKRequest();
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onError(VKError error) {
                super.onError(error);
                onUpdated(null, com.freeshop.admin.api.Error.SERVICE_ERROR);
            }

            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                onUpdated(response, com.freeshop.admin.api.Error.NO_ERROR);
            }
        });
    }

    @Override
    public void parse(Object object) {
        albums.clear();
        if (object instanceof VKResponse) {
            VKResponse response = (VKResponse) object;
            JSONObject responseJson = null;
            try {
                responseJson = response.json.getJSONObject("response");
                JSONArray items = ParseUtils.objToJSONArray(responseJson.get("items"));
                for (int i = 0; i < items.length(); i++) {
                    JSONObject jsonObject = items.getJSONObject(i);

                    Long albumId = ParseUtils.objToLong(jsonObject.get("id"));
                    Long thumbId = ParseUtils.objToLong(jsonObject.get("thumb_id"));
                    Long ownerId = ParseUtils.objToLong(jsonObject.get("owner_id"));

                    String type = "album";
                    if (jsonObject.has("type")){
                         type = ParseUtils.objToStr(jsonObject.get("type"));
                    }

                    String title = ParseUtils.objToStr(jsonObject.get("title"));

                    String description = "";
                    if (jsonObject.has("description"))
                        description = ParseUtils.objToStr(jsonObject.get("description"));

                    int size = ParseUtils.objToInt(jsonObject.get("size"));

                    JSONArray photoSizesJsonArray = ParseUtils.objToJSONArray(jsonObject.get("sizes"));
                    PhotoSizes photoSizes = new PhotoSizes(photoSizesJsonArray);

                    Album album = new Album(albumId, type, ownerId);
                    album.setThumbId(thumbId);
                    album.setTitle(title);
                    album.setDescription(description);
                    album.setSize(size);
                    album.setPhotoSizes(photoSizes);
                    albums.add(album);
                }
            } catch (JSONException e) {
                Log.e("exception while parsing album", e);
                e.printStackTrace();
            }
        }
    }

    public Album getAlbum(long id) {
        for (Album album : albums) {
            if (Math.abs(album.getId()) == id) {
                return album;
            }
        }
        return null;
    }


    @Override
    protected long getUpdatePeriod() {
        return 20 * 60 * 1000;
    }
}
