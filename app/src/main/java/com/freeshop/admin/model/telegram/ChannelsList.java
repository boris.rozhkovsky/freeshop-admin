package com.freeshop.admin.model.telegram;

import com.freeshop.admin.BuildConfig;
import com.freeshop.admin.utils.ParseUtils;

import java.util.ArrayList;
import java.util.List;

public class ChannelsList {

    private static final Channel rentTest = new Channel("Сдам/Сниму","freeshoptestchannel");
    private static final Channel buyTest = new Channel("Куплю/Продам","freeshoptestchannel");

    private static final Channel snimyProd = new Channel("#сниму@freeshopdom","snimufreeshopdom");
    private static final Channel sdamProd = new Channel("#сдам@freeshopdom","sdamfreeshopdom");
    private static final Channel podseliyProd = new Channel("#подселю@freeshopdom","arendakomnat");
    private static final Channel podesliusProd = new Channel("#подселюсь@freeshopdom","sosedifreeshopdom");

    public static ArrayList<Channel> getList(){
        ArrayList<Channel> result = new ArrayList<>();
        if (BuildConfig.BUILD_FLAVOUR.equalsIgnoreCase("prod")){
            result.add(snimyProd);
            result.add(sdamProd);
            result.add(podseliyProd);
            result.add(podesliusProd);
        } else {
            result.add(rentTest);
            result.add(buyTest);
        }
        return result;
    }
}
