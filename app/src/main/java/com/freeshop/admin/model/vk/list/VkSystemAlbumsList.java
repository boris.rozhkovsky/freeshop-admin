package com.freeshop.admin.model.vk.list;

import com.freeshop.admin.Config;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

public class VkSystemAlbumsList extends VkUpdatableAlbumsList {


    private static VkSystemAlbumsList instance;

    public static VkSystemAlbumsList getInstance() {
        synchronized (VkUpdatableAlbumsList.class) {
            if (instance == null) {
                instance = new VkSystemAlbumsList();
                instance.update();
            }
            if (instance.needsUpdate()) {
                instance.update();
            }
            return instance;
        }
    }

    @Override
    protected VKRequest getVKRequest() {
        VKRequest request = new VKRequest("photos.getAlbums", VKParameters.from("owner_id", Config.currentGroup.getGroupId(),
                "need_covers", "1",
                "photo_sizes", "1",
                "need_system", "1"));
        return request;
    }

}
