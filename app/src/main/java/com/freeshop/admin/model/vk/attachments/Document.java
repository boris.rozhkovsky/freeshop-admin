package com.freeshop.admin.model.vk.attachments;

import com.vk.sdk.api.model.VKApiDocument;

/**
 * Created by User on 08.05.2016.
 */
public class Document extends Attachment {

    private String photo100;
    private String photo130;
    private String doc;

    public Document(VKApiDocument document) {
        super(document.id, document.getType(), document.owner_id);
        this.photo100 = document.photo_100;
        this.photo130 = document.photo_130;
        this.doc = document.url;

    }

    @Override
    public String getPreviewImageUrl() {
        if (photo130 != null) {
            return photo130;
        }
        return photo100;
    }

    @Override
    public String getImageUrl() {
        if (doc != null) {
            return doc;
        }
        if (photo130 != null) {
            return photo130;
        }
        return photo100;
    }
}
