package com.freeshop.admin.model.vk.attachments;

import com.freeshop.admin.utils.ParseUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class PhotoSize {

    private String src;
    private int width;
    private int height;

    public PhotoSize(JSONObject object) {
        try {
            src = ParseUtils.objToStr(object.get("src"));
            width = ParseUtils.objToInt(object.get("width"));
            height = ParseUtils.objToInt(object.get("height"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getHeight() {
        return height;
    }

    public String getSrc() {
        return src;
    }

    public int getWidth() {
        return width;
    }
}
