package com.freeshop.admin.model.telegram;

public class Bot {

    private String screenName;
    private String path;

    public Bot(String screenName, String path) {
        this.screenName = screenName;
        this.path = path;
    }

    public String getScreenName() {
        return screenName;
    }

    public String getPath() {
        return path;
    }
}
