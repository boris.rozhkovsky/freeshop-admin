package com.freeshop.admin.model.vk.attachments;

import android.net.Uri;

import com.freeshop.admin.api.tasks.UploadPhotoTask;

public class UploadAttachment extends Attachment {

    private Uri uri;
    private UploadPhotoTask task;

    public UploadAttachment(long id, Uri uri, String type, long ownerId) {
        super(id, type, ownerId);
        this.uri = uri;
    }

    @Override
    public String getPreviewImageUrl() {
        return uri.toString();
    }

    @Override
    public String getImageUrl() {
        return uri.toString();
    }
}
