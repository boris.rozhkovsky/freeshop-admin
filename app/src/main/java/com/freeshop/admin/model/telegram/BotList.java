package com.freeshop.admin.model.telegram;

import com.freeshop.admin.BuildConfig;

public class BotList {

    private static final Bot testBot = new Bot("Тестовый бот", "bot196346602:AAGh5sPHifi5MowU79u1JnZ32cb8aYC6Z6Q");
    private static final Bot prodBot = new Bot("Боевой бот", "bot225537145:AAHrbAHudJNAMnNJXuRpbkd2oMA9zXzMwu4");


    public static Bot getBot() {
        if (BuildConfig.BUILD_FLAVOUR.equalsIgnoreCase("prod")) {
            return prodBot;
        } else {
            return testBot;
        }
    }
}
