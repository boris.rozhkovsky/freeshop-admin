package com.freeshop.admin.model;

import android.os.Handler;
import android.os.Looper;

import com.freeshop.admin.api.Error;

import org.joda.time.DateTime;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public abstract class UpdatableModel {

    protected static Handler mainLoopHanlder = new Handler(Looper.getMainLooper());

    protected long lastUpdated;
    private boolean updateInProgress = false;
    private transient List<UpdateListener<? extends UpdatableModel>> listeners;

    protected UpdatableModel() {
        listeners = new ArrayList<>();
    }

    public abstract void update();

    public abstract void parse(Object object);

    public void onUpdated(Object object, Error error) {
        if (error == Error.NO_ERROR) {
            parse(object);
        }
        reportUpdateFinish(error);
    }

    public boolean isUpdateInProgress() {
        return updateInProgress;
    }

    @SuppressWarnings("unchecked")
    protected void reportUpdateStarted() {
        mainLoopHanlder.post(new Runnable() {
            @Override
            public void run() {
                synchronized (UpdatableModel.class) {
                    for (UpdateListener updateListener : listeners)
                        updateListener.updateStarted(UpdatableModel.this);
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    protected void reportUpdateFinish(final Error error) {
        mainLoopHanlder.post(new Runnable() {
            @Override
            public void run() {
                synchronized (UpdatableModel.class) {
                    for (UpdateListener updateListener : listeners)
                        updateListener.onModelUpdated(UpdatableModel.this, error == Error.NO_ERROR, error);
                }
            }
        });
    }

    public boolean isEmpty() {
        return lastUpdated == 0;
    }

    public void addListener(UpdateListener<? extends UpdatableModel> updateListener) {
        synchronized (UpdatableModel.class) {
            listeners.add(updateListener);
        }
    }

    public void removeListener(UpdateListener updateListener) {
        synchronized (UpdatableModel.class) {
            listeners.remove(updateListener);
        }
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean needsUpdate() {
        if (updateInProgress) {
            return false;
        }

        return DateTime.now().getMillis() - getLastUpdated() > getUpdatePeriod();

    }

    protected abstract long getUpdatePeriod();

    public interface UpdateListener<T extends UpdatableModel> {
        public void updateStarted(T updatableModel);

        public void onModelUpdated(T updatableModel, boolean success, Error error);
    }
}

