package com.freeshop.admin.model.vk.list;

import com.freeshop.admin.api.Error;
import com.freeshop.admin.model.UpdatableList;
import com.freeshop.admin.model.vk.Author;
import com.freeshop.admin.model.vk.Group;
import com.freeshop.admin.model.vk.Post;
import com.freeshop.admin.model.vk.User;
import com.freeshop.admin.model.vk.attachments.Album;
import com.freeshop.admin.model.vk.attachments.Attachment;
import com.freeshop.admin.model.vk.attachments.Document;
import com.freeshop.admin.model.vk.attachments.Photo;
import com.freeshop.admin.model.vk.attachments.Video;
import com.freeshop.admin.utils.ParseUtils;
import com.vk.sdk.api.VKDefaultParser;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiDocument;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKApiPhotoAlbum;
import com.vk.sdk.api.model.VKApiPost;
import com.vk.sdk.api.model.VKApiVideo;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPostArray;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class UpdatableVkPostList extends UpdatableList<Post> {

    protected long updatePeriod = 30 * 1000;
    private List<Post> posts = new ArrayList<>();

    @Override
    protected long getUpdatePeriod() {
        return updatePeriod;
    }

    @Override
    public void update() {
        reportUpdateStarted();
        VKRequest request = getVKRequest();
        request.setResponseParser(getParser());
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onError(VKError error) {
                super.onError(error);
                onUpdated(null, com.freeshop.admin.api.Error.SERVICE_ERROR);
            }

            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                onUpdated(response, Error.NO_ERROR);
            }
        });
    }

    @Override
    public void parse(Object object) {
        posts.clear();
        if (object instanceof VKResponse) {

            VKResponse response = (VKResponse) object;
            VKPostArray postsArray = (VKPostArray) response.parsedModel;
            JSONObject jsonObject = null;
            JSONArray items = null;
            try {
                jsonObject = response.json.getJSONObject("response");
                items = ParseUtils.objToJSONArray(jsonObject.get("items"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            LinkedHashMap<Long, User> profiles = parseProfiles(response);
            LinkedHashMap<Long, Group> groups = parseGroups(response);
            LinkedHashMap<Long, Long> ownerIds = new LinkedHashMap<>();
            LinkedHashMap<Long, Boolean> canEditIds = new LinkedHashMap<>();
            LinkedHashMap<Long, Boolean> canDeleteIds = new LinkedHashMap<>();


            if (items != null) {
                ownerIds = parseOwnerIds(items);
                canDeleteIds = parseCanDelete(items);
                canEditIds = parseCanEdit(items);
            }

            for (int i = 0; i < postsArray.size(); i++) {
                VKApiPost vkApiPost = postsArray.get(i);
                Post post = parsePost(vkApiPost, profiles, groups, ownerIds, canEditIds, canDeleteIds);
                posts.add(post);
            }
        }
    }

    private Post parsePost(VKApiPost vkApiPost, LinkedHashMap<Long, User> profiles,
                           LinkedHashMap<Long, Group> groups,
                           LinkedHashMap<Long, Long> ownerIds,
                           LinkedHashMap<Long, Boolean> canEditIds,
                           LinkedHashMap<Long, Boolean> canDeleteIds) {

        Post post = new Post(vkApiPost.getId(), vkApiPost.text);
        post.setFromId(Long.valueOf(vkApiPost.from_id));
        post.setTime(new DateTime(vkApiPost.date * 1000));
        post.setAttachments(getAttachments(vkApiPost));
        post.setPostType(Post.PostType.fromString(vkApiPost.post_type));

        Author from = getAuthor(vkApiPost.from_id, profiles, groups);
        Long ownerId = ownerIds.get(Long.valueOf(vkApiPost.id));
        Author owner = null;
        if (ownerId != null) {
            owner = getAuthor(ownerId, profiles, groups);
        }

        Boolean canEdit = canEditIds.get(Long.valueOf(vkApiPost.id));
        Boolean canDelete = canDeleteIds.get(Long.valueOf(vkApiPost.id));


        if (canEdit == null)
            canEdit = false;

        if (canDelete == null)
            canDelete = false;

        post.setCanDelete(canDelete);
        post.setCanEdit(canEdit);

        Author signer = null;

        if (vkApiPost.signer_id != 0) {
            signer = getAuthor(vkApiPost.signer_id, profiles, groups);
        } else if (vkApiPost.copy_history != null && vkApiPost.copy_history.size() > 0) {
            List<Post> copyHistory = new ArrayList<>();
            for (VKApiPost copyVkPost : vkApiPost.copy_history) {
                copyHistory.add(parsePost(copyVkPost, profiles, groups, ownerIds, canEditIds, canDeleteIds));
            }
            post.setCopyHistory(copyHistory);
        }

        post.setSigner(signer);
        post.setOwner(owner);
        post.setFrom(from);
        return post;
    }

    private Author getAuthor(long id, LinkedHashMap<Long, User> profiles, LinkedHashMap<Long, Group> groups) {
        Author result = null;
        if (id >= 0) {
            result = profiles.get(id);
        } else {
            result = groups.get(-id);
        }
        return result;
    }

    private List<Attachment> getAttachments(VKApiPost post) {
        List<Attachment> attachments = new ArrayList<>();
        for (int i = 0; i < post.attachments.size(); i++) {
            VKAttachments.VKApiAttachment attachment = post.attachments.get(i);
            if (attachment.getType().equalsIgnoreCase("photo")) {
                VKApiPhoto vkApiPhoto = (VKApiPhoto) attachment;
                Photo photo = new Photo(vkApiPhoto);
                attachments.add(photo);
            } else if (attachment.getType().equalsIgnoreCase("video")) {
                VKApiVideo vkApivideo = (VKApiVideo) attachment;
                Video video = new Video(vkApivideo);
                attachments.add(video);
            } else if (attachment.getType().equalsIgnoreCase("album")) {
                VKApiPhotoAlbum vkApiAlbum = (VKApiPhotoAlbum) attachment;

                Album album = VkAlbumsList.getInstance().getAlbum(Long.valueOf(vkApiAlbum.id));

                if (album == null) {
                    album = VkSystemAlbumsList.getInstance().getAlbum(Long.valueOf(vkApiAlbum.id));
                }

                if (album != null) {
                    attachments.add(album);
                }
            } else if (attachment.getType().equalsIgnoreCase("doc")){
                Document doc = new Document((VKApiDocument) attachment);
                attachments.add(doc);
            }

        }
        return attachments;
    }

    private LinkedHashMap<Long, User> parseProfiles(VKResponse response) {
        LinkedHashMap<Long, User> parsedProfiles = new LinkedHashMap();
        try {
            JSONObject responseJson = response.json.getJSONObject("response");
            JSONArray profiles = ParseUtils.objToJSONArray(responseJson.getJSONArray("profiles"));

            for (int i = 0; i < profiles.length(); i++) {
                JSONObject jsonObject = profiles.getJSONObject(i);
                Long userId = jsonObject.getLong("id");
                String name = jsonObject.getString("first_name");
                String surname = jsonObject.getString("last_name");
                String photo50 = jsonObject.getString("photo_50");
                String photo100 = jsonObject.getString("photo_100");

                User user = new User(userId, name, surname);
                user.setPhoto50(photo50);
                user.setPhoto100(photo100);
                parsedProfiles.put(userId, user);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parsedProfiles;
    }

    private LinkedHashMap<Long, Group> parseGroups(VKResponse response) {
        LinkedHashMap<Long, Group> parsedGroups = new LinkedHashMap();
        try {
            JSONObject responseJson = response.json.getJSONObject("response");
            JSONArray profiles = ParseUtils.objToJSONArray(responseJson.getJSONArray("groups"));

            for (int i = 0; i < profiles.length(); i++) {
                JSONObject jsonObject = profiles.getJSONObject(i);
                Long groupId = jsonObject.getLong("id");
                String name = jsonObject.getString("name");
                String photo50 = jsonObject.getString("photo_50");
                String photo100 = jsonObject.getString("photo_100");

                Group group = new Group(groupId);
                group.setName(name);
                group.setPhoto50(photo50);
                group.setPhoto100(photo100);
                parsedGroups.put(groupId, group);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parsedGroups;
    }

    private LinkedHashMap<Long, Long> parseOwnerIds(JSONArray jsonArray) {
        LinkedHashMap<Long, Long> result = new LinkedHashMap<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject item = jsonArray.getJSONObject(i);
                long id = ParseUtils.objToLong(item.get("id"));
                long ownerId = ParseUtils.objToLong(item.get("owner_id"));
                result.put(id, ownerId);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private LinkedHashMap<Long, Boolean> parseCanDelete(JSONArray jsonArray) {
        LinkedHashMap<Long, Boolean> result = new LinkedHashMap<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject item = jsonArray.getJSONObject(i);
                boolean canDelete = false;
                long id = ParseUtils.objToLong(item.get("id"));
                if (item.has("can_delete")) {
                    canDelete = ParseUtils.objToBoolean(item.get("can_delete"));
                }
                result.put(id, canDelete);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private LinkedHashMap<Long, Boolean> parseCanEdit(JSONArray jsonArray) {
        LinkedHashMap<Long, Boolean> result = new LinkedHashMap<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject item = jsonArray.getJSONObject(i);
                boolean canDelete = false;
                long id = ParseUtils.objToLong(item.get("id"));
                if (item.has("can_edit")) {
                    canDelete = ParseUtils.objToBoolean(item.get("can_edit"));
                }
                result.put(id, canDelete);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    @Override
    public List<Post> getItems() {
        return posts;
    }

    public Post getPost(long id) {
        for (Post post : posts) {
            if (post.getId() == id) {
                return post;
            }
        }
        return null;
    }

    protected abstract VKRequest getVKRequest();

    protected abstract VKDefaultParser getParser();

    public int getTodayPostsCount(){
        int count = 0;
        Interval today = new Interval(DateTime.now().withTimeAtStartOfDay(), Days.ONE);
        for (Post post : getItems()){
            if (today.contains(post.getTime().getMillis())){
                count++;
            }
        }
        return count;
    }
}
