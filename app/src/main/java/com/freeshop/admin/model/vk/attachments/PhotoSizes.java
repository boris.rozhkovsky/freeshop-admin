package com.freeshop.admin.model.vk.attachments;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PhotoSizes {
    private List<PhotoSize> photoSizes = new ArrayList<>();

    public PhotoSizes(JSONArray jsonArray) {
        for (int j = 0; j < jsonArray.length(); j++) {
            try {
                JSONObject photoObject = jsonArray.getJSONObject(j);
                PhotoSize photoSize = new PhotoSize(photoObject);
                photoSizes.add(photoSize);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public PhotoSize getBiggest() {
        if (photoSizes.size() == 0) {
            return null;
        }
        return photoSizes.get(photoSizes.size() - 1);
    }

    public PhotoSize getForWidth(int width) {
        for (PhotoSize photoSize : photoSizes) {
            if (photoSize.getWidth() > width) {
                return photoSize;
            }
        }
        return getBiggest();
    }
}
