package com.freeshop.admin.model.vk.list;

import com.freeshop.admin.Config;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;

public class VkAlbumsList extends VkUpdatableAlbumsList {


    private static VkAlbumsList instance;

    public static VkAlbumsList getInstance() {
        synchronized (VkUpdatableAlbumsList.class) {
            if (instance == null) {
                instance = new VkAlbumsList();
                instance.update();
            }
            if (instance.needsUpdate()) {
                instance.update();
            }
            return instance;
        }
    }

    @Override
    protected VKRequest getVKRequest() {
        VKRequest request = new VKRequest("photos.getAlbums", VKParameters.from("owner_id", Config.currentGroup.getGroupId(),
                "need_covers", "1",
                "photo_sizes", "1"));
        return request;
    }

}
