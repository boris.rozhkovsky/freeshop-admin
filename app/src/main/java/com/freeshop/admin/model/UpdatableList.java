package com.freeshop.admin.model;

import java.util.List;

public abstract class UpdatableList<T> extends UpdatableModel {

    public abstract List<T> getItems();

}
