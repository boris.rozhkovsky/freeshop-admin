package com.freeshop.admin.model.vk;

public class User extends Author {

    private String name;
    private String surname;
    private String photo50;
    private String photo100;

    public User(long id, String name, String surname) {
        super(id);
        this.name = name;
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPhoto50() {
        return photo50;
    }

    public void setPhoto50(String photo50) {
        this.photo50 = photo50;
    }

    public String getPhoto100() {
        return photo100;
    }

    public void setPhoto100(String photo100) {
        this.photo100 = photo100;
    }

    @Override
    public String getScreenName() {
        return String.format("%s %s", name, surname);
    }

    @Override
    public String getAvatarUrl() {
        return getPhoto100();
    }
}
