package com.freeshop.admin.model.vk;

public class Group extends Author {

    private String name;

    private String photo50;
    private String photo100;

    public Group(long id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto50() {
        return photo50;
    }

    public void setPhoto50(String photo50) {
        this.photo50 = photo50;
    }

    public String getPhoto100() {
        return photo100;
    }

    public void setPhoto100(String photo100) {
        this.photo100 = photo100;
    }

    @Override
    public String getScreenName() {
        return getName();
    }

    @Override
    public String getAvatarUrl() {
        return getPhoto100();
    }
}
