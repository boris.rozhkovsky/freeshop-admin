package com.freeshop.admin.model.vk.list;


import com.freeshop.admin.Config;
import com.freeshop.admin.model.vk.Post;
import com.vk.sdk.api.VKDefaultParser;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.model.VKPostArray;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;


public class NewsFeedPostList extends UpdatableVkPostList {


    private static NewsFeedPostList instance;

    public static NewsFeedPostList getInstance() {
        synchronized (NewsFeedPostList.class) {
            if (instance == null) {
                instance = new NewsFeedPostList();
                instance.update();
            }
            if (instance.needsUpdate()) {
                instance.update();
            }
            return instance;
        }
    }

    private NewsFeedPostList(){

    }

    @Override
    protected VKRequest getVKRequest() {
        return new VKRequest("wall.get", VKParameters.from("owner_id", Config.currentGroup.getGroupId(), "count","100","extended","1"));
    }

    @Override
    protected VKDefaultParser getParser() {
        return new VKDefaultParser(VKPostArray.class);
    }

}
