package com.freeshop.admin.model.vk.attachments;

import com.freeshop.admin.model.vk.VkModel;

public abstract class Attachment extends VkModel {

    private String type;
    private long ownerId;

    public Attachment(long id, String type, long ownerId) {
        super(id);
        this.type = type;
        this.ownerId = ownerId;
    }

    public abstract String getPreviewImageUrl();
    public abstract String getImageUrl();


    public String getType() {
        return type;
    }

    public long getOwnerId() {
        return ownerId;
    }

}
