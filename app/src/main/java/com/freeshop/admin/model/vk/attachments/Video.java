package com.freeshop.admin.model.vk.attachments;

import com.vk.sdk.api.model.VKApiVideo;

public class Video extends Attachment {

    private String photo130;
    private String photo320;
    private String photo640;

    public Video(VKApiVideo vkApiVideo) {
        super(vkApiVideo.id, vkApiVideo.getType(),vkApiVideo.owner_id);
        photo130 = vkApiVideo.photo_130;
        photo320 = vkApiVideo.photo_320;
        photo640 = vkApiVideo.photo_640;
    }

    @Override
    public String getPreviewImageUrl() {
        return photo320;
    }

    @Override
    public String getImageUrl() {
        return photo640;
    }
}
