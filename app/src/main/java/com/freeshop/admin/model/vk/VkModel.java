package com.freeshop.admin.model.vk;

public class VkModel {

   protected long id;

    public VkModel(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
