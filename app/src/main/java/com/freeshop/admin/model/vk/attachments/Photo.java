package com.freeshop.admin.model.vk.attachments;

import android.text.TextUtils;

import com.vk.sdk.api.model.VKApiPhoto;

import java.util.ArrayList;
import java.util.List;

public class Photo extends Attachment {

    private String photo75;
    private String photo130;
    private String photo604;
    private String photo807;
    private String photo1280;
    private String photo2560;

    private List<String> photoUrls = new ArrayList<>();

    public Photo(VKApiPhoto vkPhoto) {
        super(vkPhoto.getId(), vkPhoto.getType(), vkPhoto.owner_id);
        photo75 = vkPhoto.photo_75;
        photo130 = vkPhoto.photo_130;
        photo604 = vkPhoto.photo_604;
        photo807 = vkPhoto.photo_807;
        photo1280 = vkPhoto.photo_1280;
        photo2560 = vkPhoto.photo_2560;

        photoUrls.add(photo75);
        photoUrls.add(photo130);
        photoUrls.add(photo604);
        photoUrls.add(photo807);
        photoUrls.add(photo1280);
        photoUrls.add(photo2560);
    }

    public String getPhoto75() {
        return photo75;
    }

    public String getPhoto130() {
        return photo130;
    }

    public String getPhoto604() {
        return photo604;
    }

    public String getPhoto807() {
        return photo807;
    }

    public String getPhoto1280() {
        return photo1280;
    }

    public String getPhoto2560() {
        return photo2560;
    }

    @Override
    public String getPreviewImageUrl() {
        for (int i = 2; i < photoUrls.size(); i++) {
            if (photoUrls.get(i) != null && !TextUtils.isEmpty(photoUrls.get(i))) {
                return photoUrls.get(i);
            }
        }
        return "";
    }

    @Override
    public String getImageUrl() {
        for (int i = photoUrls.size()-2; i >= 0; i--) {
            if (photoUrls.get(i) != null && !TextUtils.isEmpty(photoUrls.get(i))) {
                return photoUrls.get(i);
            }
        }
        if (getPhoto2560()!=null && !TextUtils.isEmpty(getPhoto2560())) {
            return getPhoto2560();
        }
        return "";
    }
}
