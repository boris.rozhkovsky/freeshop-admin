package com.freeshop.admin.model.vk.list;


import com.freeshop.admin.Config;
import com.vk.sdk.api.VKDefaultParser;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.model.VKPostArray;



public class SuggestionsPostList extends UpdatableVkPostList {

    private static SuggestionsPostList instance;

    public static SuggestionsPostList getInstance() {
        synchronized (SuggestionsPostList.class) {
            if (instance == null) {
                instance = new SuggestionsPostList();
                instance.update();
            }
            if (instance.needsUpdate()) {
                instance.update();
            }
            return instance;
        }
    }

    private SuggestionsPostList(){

    }


    @Override
    protected VKRequest getVKRequest() {
        return new VKRequest("wall.get", VKParameters.from("owner_id", Config.currentGroup.getGroupId(),
                "filter", "suggests",
                "count","100",
                "extended","1"));
    }

    @Override
    protected VKDefaultParser getParser() {
        return new VKDefaultParser(VKPostArray.class);
    }

}
