package com.freeshop.admin.model.vk;

public abstract class Author  extends VkModel {

    public Author(long id) {
        super(id);
    }

    public abstract String getScreenName();
    public abstract String getAvatarUrl();
}
