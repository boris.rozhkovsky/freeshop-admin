package com.freeshop.admin.model.vk.attachments;

import java.security.acl.Owner;
import java.util.List;

public class Album extends Attachment {

    private PhotoSizes photoSizes;
    private long thumbId;
    private String title;
    private String description;
    private int size;

    public Album(Long id, String type, long ownerId) {
        super(id, type, ownerId);
    }

    @Override
    public String getPreviewImageUrl() {
        PhotoSize size = photoSizes.getForWidth(400);
        if (size!=null){
            return size.getSrc();
        }
       return "";
    }

    @Override
    public String getImageUrl() {
        PhotoSize size = photoSizes.getBiggest();
        if (size!=null){
            return size.getSrc();
        }
        return "";
    }


    public long getThumbId() {
        return thumbId;
    }

    public void setThumbId(long thumbId) {
        this.thumbId = thumbId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setPhotoSizes(PhotoSizes photoSizes) {
        this.photoSizes = photoSizes;
    }
}
