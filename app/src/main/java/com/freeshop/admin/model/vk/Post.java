package com.freeshop.admin.model.vk;

import com.freeshop.admin.model.vk.attachments.Attachment;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Post extends VkModel {

    private String text;
    private Long fromId;
    private DateTime time;
    private List<Attachment> attachments = new ArrayList<Attachment>();
    private Author owner;
    private Author from;
    private Author signer;
    private List<Post> copyHistory;
    private PostType postType;
    private boolean canEdit;
    private boolean canDelete;

    public Post(long id, String text) {
        super(id);
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Long getFromId() {
        return fromId;
    }

    public void setFromId(Long fromId) {
        this.fromId = fromId;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void setSigner(Author signer) {
        this.signer = signer;
    }

    public Author getOwner() {
        return owner;
    }

    public void setOwner(Author owner) {
        this.owner = owner;
    }

    public Author getFrom() {
        return from;
    }

    public void setFrom(Author from) {
        this.from = from;
    }

    public List<Post> getCopyHistory() {
        return copyHistory;
    }

    public void setCopyHistory(List<Post> copyHistory) {
        this.copyHistory = copyHistory;
    }

    public Author getSigner() {
        return signer;
    }


    public void setText(String text) {
        this.text = text;
    }

    public PostType getPostType() {
        return postType;
    }

    public void setPostType(PostType postType) {
        this.postType = postType;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public void addAttachment(Attachment attachment) {
        this.attachments.add(attachment);
    }

    public String toTelegramString() {
        return toTelegramString("");
    }

    static DateTimeFormatter formatter = DateTimeFormat.forPattern(" HH:mm dd.MM.yy");

    public String toTelegramString(String appended) {
        StringBuilder builder = new StringBuilder();

        if (copyHistory != null && copyHistory.size() > 0) {
            builder.append(copyHistory.get(0).toTelegramString(" >"));
        }

        builder.append(appended + getText());
        builder.append("\n");

        int imageNumber = 1;
        if (attachments.size() > 0) {
            builder.append("|  ");
        }
        for (Attachment attachment : attachments) {
            builder.append(appended + toUrl(attachment.getImageUrl(), String.valueOf(imageNumber)) + "  |  ");
            imageNumber++;
        }
        builder.append("\n\n");

        if (signer != null) {
            builder.append(appended + toUrl("https://vk.com/id" + signer.getId(), signer.getScreenName()) + " \n");
        }

        builder.append(appended + toItalic(formatter.print(getTime())));

        return builder.toString();
    }

    private static String toUrl(String url, String name) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(name);
        builder.append("]");
        builder.append("(");
        builder.append(url);
        builder.append(")");
        return builder.toString();
    }

    private static String toItalic(String text) {
        StringBuilder builder = new StringBuilder();
        builder.append("_");
        builder.append(text);
        builder.append("_");
        return builder.toString();
    }

    private static String toBold(String text) {
        StringBuilder builder = new StringBuilder();
        builder.append("*");
        builder.append(text);
        builder.append("*");
        return builder.toString();
    }

    private static String replaceKeywords(String text) {
        for (String keyword : keywords) {
            text = text.replaceAll("(?i)" + " " +  keyword, toBold(keyword).toUpperCase());
        }
        return text;
    }

    private static List<String> keywords = Arrays.asList("сдаю", "район", "срок", "кому", "описание", "связь", "комнат", "цена", "о себе", "получаю", "способ связи", "место", "положение" );

    public enum PostType {
        SUGGEST("suggest"),
        POST("post");

        private String label;

        private PostType(String label) {
            this.label = label;
        }

        public static PostType fromString(String label) {
            for (PostType type : PostType.values()) {
                if (type.label.equalsIgnoreCase(label)) {
                    return type;
                }
            }
            return null;
        }
    }

    public String getAttachmentsString() {
        return getAttachmentsString(this.attachments);
    }

    public static String getAttachmentsString(List<Attachment> attachments) {
        StringBuilder builder = new StringBuilder();
        for (Attachment attachment : attachments) {
            builder.append(attachment.getType());
            builder.append(attachment.getOwnerId());
            builder.append("_");
            builder.append(attachment.getId());

            if (!attachment.equals(attachments.get(attachments.size() - 1))) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

}
