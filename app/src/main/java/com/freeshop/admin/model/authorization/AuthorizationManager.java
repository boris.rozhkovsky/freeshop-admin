package com.freeshop.admin.model.authorization;

import com.freeshop.admin.model.vk.User;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKList;

public class AuthorizationManager {

    private static AuthorizationManager instance = null;

    private AuthorizationManager() {

    }

    public static AuthorizationManager getInstance() {
        if (instance == null) {
            instance = new AuthorizationManager();
        }
        return instance;
    }

    private User currentUser;

    private VKAccessToken accessToken;

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public VKAccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(VKAccessToken accessToken) {
        this.accessToken = accessToken;
    }

    public void updateCurrentUser() {
        VKRequest request = VKApi.users().get();
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onError(VKError error) {
                super.onError(error);
            }

            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKList<VKApiUser> users = (VKList<VKApiUser>) response.parsedModel;
                setCurrentUser(new User(users.get(0).getId(), users.get(0).first_name, users.get(0).last_name));
            }
        });
    }
}
