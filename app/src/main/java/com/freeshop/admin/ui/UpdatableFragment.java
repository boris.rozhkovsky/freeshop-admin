package com.freeshop.admin.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.freeshop.admin.R;
import com.freeshop.admin.api.Error;
import com.freeshop.admin.model.UpdatableModel;
import com.freeshop.admin.utils.Utils;

public abstract class UpdatableFragment<T extends UpdatableModel> extends BaseFragment implements UpdatableModel.UpdateListener<T>, SwipeRefreshLayout.OnRefreshListener {

    protected T updatableModel;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected FrameLayout frameLayout;
    protected View emptyData;
    protected View noDataofflineModeIndicator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.updatableModel = getUpdatableModel();
        if (updatableModel == null)
            throw new NullPointerException(UpdatableModel.class + " cannot be null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        LinearLayout rootView = new LinearLayout(getActivity());
        rootView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        rootView.setOrientation(LinearLayout.VERTICAL);
        rootView.setBackgroundColor(getResources().getColor(R.color.foggy));

        frameLayout = new FrameLayout(getActivity());
        frameLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        swipeRefreshLayout = new SwipeRefreshLayout(getActivity(), null);
        emptyData = getEmptyDataView(inflater);
        emptyData.setVisibility(View.GONE);
        noDataofflineModeIndicator = getOfflineModeView(inflater);
        noDataofflineModeIndicator.setVisibility(View.GONE);

        swipeRefreshLayout.addView(getContentView(inflater, swipeRefreshLayout, savedInstanceState));
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.darkish_green,
                R.color.dark_green,
                R.color.sky_green);

        frameLayout.addView(swipeRefreshLayout);
        frameLayout.addView(emptyData);
        frameLayout.addView(noDataofflineModeIndicator);
        rootView.addView(frameLayout);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updatableModel.addListener(this);
        if (updatableModel.needsUpdate()) {
            updateModel();
        }
        updateView(updatableModel);
        refreshEmptyDataView();
    }

    @Override
    public void onPause() {
        super.onPause();
        updatableModel.removeListener(this);
        setInterdemidiateProgress(false);
    }

    @Override
    public void updateStarted(T updatableModel) {
        if (!swipeRefreshLayout.isRefreshing())
            setInterdemidiateProgress(true);
    }

    protected boolean modelIsEmpty() {
        return false;
    }

    @Override
    public void onModelUpdated(T updatableModel, boolean success, Error error) {
        setInterdemidiateProgress(false);
        if (error == Error.NO_ERROR) {
            updateView(updatableModel);
        } else {
            showUpdateError(error);
        }

        refreshEmptyDataView();
    }

    protected void refreshEmptyDataView() {
        if (!updatableModel.isUpdateInProgress() && modelIsEmpty()) {
            emptyData.setVisibility(View.VISIBLE);
        } else {
            emptyData.setVisibility(View.GONE);
        }
    }

    protected void setInterdemidiateProgress(boolean enabled) {
        if (enabled) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        if (Utils.isInternetAvailable(getActivity())) {
            updatableModel.update();
        } else {
            Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_SHORT).show();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    protected void updateModel() {
        if (!updatableModel.isUpdateInProgress()) {
            swipeRefreshLayout.setRefreshing(false);
            updatableModel.update();
        }
    }

    /**
     * Create and return inner view that will show UpdatableModel.
     */
    protected abstract View getContentView(LayoutInflater inflater,
                                           @Nullable ViewGroup container,
                                           @Nullable Bundle savedInstanceState);

    /**
     * Instantiate and return UpdatableModel that will be shown in this fragment.
     */
    protected abstract T getUpdatableModel();


    protected abstract void updateView(T updatableModel);
    /**
     * Will be called when current UpdatableModel view needs to be updated.
     */
    /**
     * Returns empty Data View to be shown when there is no data
     */
    protected View getEmptyDataView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.offline_mode_view, null);
    }

    /**
     * Override this method to show error message when current UpdatableModel failed to update.
     * Toast with error message will be shown by default;
     */
    protected void showUpdateError(@NonNull Error error) {
        if (Utils.isInternetAvailable(getActivity()) && (updatableModel == null || updatableModel.isEmpty())) {
            emptyData.setVisibility(View.VISIBLE); // отображать нечего - выводим заглушку
        }
    }

    protected View getOfflineModeView(LayoutInflater inflater) {
        return inflater.inflate(R.layout.offline_mode_view, this.frameLayout, false);
    }

}
