package com.freeshop.admin.ui.attachments;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.freeshop.admin.FreeshopAdminApplication;
import com.freeshop.admin.R;

public class FullScreenAttachmentActivity extends AppCompatActivity {

    public static final String ATTACHMENT_URL = "ATTACHMENT_URL ";
    private ImageView imageView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscreen_attachment_activity);
        imageView = (ImageView) findViewById(R.id.imageView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        String photoUrl = null;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(ATTACHMENT_URL)) {
                photoUrl = extras.getString(ATTACHMENT_URL);
            }
        }

        Glide.with(FreeshopAdminApplication.getAppContext())
                .load(photoUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setIndeterminate(false);
                        return false;
                    }
                })
                .into(imageView);
    }
}
