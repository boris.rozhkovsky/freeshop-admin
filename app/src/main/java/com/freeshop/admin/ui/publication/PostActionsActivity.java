package com.freeshop.admin.ui.publication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.freeshop.admin.ui.BaseActivity;

public class PostActionsActivity extends BaseActivity {

    public static String POST_ID_EXTRA = "POST_ID_EXTRA";

    private PostActionsFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long postId = getIntent().getLongExtra(POST_ID_EXTRA, 0);
        fragment = PostActionsFragment.getInstance(postId);
        setCurrentFragment(fragment, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            fragment.onImageSelected(uri);
        }
    }

}
