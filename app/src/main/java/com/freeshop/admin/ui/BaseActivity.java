package com.freeshop.admin.ui;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.freeshop.admin.R;


public class BaseActivity extends AppCompatActivity {

    private static BaseActivity currentActivity;
    protected AppBarLayout appBarLayout;
    protected Toolbar toolbar;
    protected FrameLayout tabContainer;
    protected CoordinatorLayout root;
    protected FrameLayout fragmentContainer;
    protected FrameLayout toolbarRightContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBarLayout = (AppBarLayout) findViewById(R.id.appBarlayout);
        tabContainer = (FrameLayout) findViewById(R.id.tabContainer);
        root = (CoordinatorLayout) findViewById(R.id.root);
        fragmentContainer = (FrameLayout) findViewById(R.id.container);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);

        toolbar.setTitleTextColor(Color.BLACK);

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.black_54));
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbarRightContainer = new FrameLayout(this);
        toolbar.addView(toolbarRightContainer);
        Toolbar.LayoutParams params = (Toolbar.LayoutParams) toolbarRightContainer.getLayoutParams();
        int marginRight = (int) getResources().getDimension(R.dimen.toolbar_margin_right);
        params.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        params.setMargins(0, 0, marginRight, 0);

    }

    protected void setCurrentFragment(BaseFragment fragment, boolean updateTitle) {
        setCurrentFragment(fragment, updateTitle, null, null);
    }

    protected void setCurrentFragment(BaseFragment fragment, boolean updateTitle, Integer animationOut, Integer animationIn) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (animationIn != null && animationOut != null) {
            transaction.setCustomAnimations(animationIn, animationOut);
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        } else {
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        }
        getSupportFragmentManager().executePendingTransactions();

        if (updateTitle) {
            setTitle(fragment.getTitle());
        }

        setTabLayout(fragment.getTabLayout());
    }

    protected void setTabLayout(TabLayout tabLayout) {
        tabContainer.removeAllViews();
        if (tabLayout != null) {
            tabContainer.addView(tabLayout);
        }
    }

    public void setTitle(String title) {
        if (title != null) {
            toolbar.setTitle(title);
        } else {
            toolbar.setTitle("");
        }
    }

    public void setRightToolbarView(View v) {
        toolbarRightContainer.removeAllViews();
        toolbarRightContainer.addView(v);
    }

    public static void setCurrentActivity(BaseActivity activity) {
        currentActivity = activity;
    }

    public static BaseActivity getCurrentActivity() {
        return currentActivity;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (currentActivity == this) {
            setCurrentActivity(null);
        }
    }
}
