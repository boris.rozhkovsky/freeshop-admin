package com.freeshop.admin.ui.utils;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;

import com.freeshop.admin.R;
import com.freeshop.admin.ui.BaseActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class DatePicker extends Picker implements DatePickerDialog.OnDateSetListener {

    private LocalDate date = null;
    private DateTime maxDate;

    private DateTimeFormatter formatter;

    private boolean onlyFutureDates = false;
    private List<WeakReference<OnDateSelectedListener>> onDateSelectedListeners = new ArrayList<>();

    public DatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @Override
    protected void onClick() {
        int year = 0;
        int month = 0;
        int dayOfMonth = 0;

        if (date != null) {
            year = date.getYear();
            month = date.getMonthOfYear() - 1;
            dayOfMonth = date.getDayOfMonth();
        } else {
            Calendar now = Calendar.getInstance();
            year = now.get(Calendar.YEAR);
            month = now.get(Calendar.MONTH);
            dayOfMonth = now.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog dialog = DatePickerDialog.newInstance(
                DatePicker.this,
                year,
                month,
                dayOfMonth
        );
        dialog.autoDismiss(true);
        dialog.setMaxDate(maxDate.toCalendar(null));

        if (onlyFutureDates) {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 1);
            dialog.setMinDate(c);
        }

        dialog.setThemeDark(false);
        dialog.setAccentColor(getContext().getResources().getColor(R.color.dark_green));
        dialog.show(BaseActivity.getCurrentActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    protected void init(Context context, AttributeSet attrs) {
        super.init(context, attrs);
        formatter = DateTimeFormat.forPattern("dd MMMM yyyy");

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        this.date = new LocalDate(year, monthOfYear + 1, dayOfMonth);
        updateView();
        for (WeakReference<OnDateSelectedListener> listenerRef : onDateSelectedListeners) {
            if (listenerRef != null && listenerRef.get() != null) {
                listenerRef.get().onDateSelected(date);
            }
        }
    }

    private void updateView() {
        String text = "";
        if (isToday()) {
            text = getContext().getResources().getString(R.string.today);
        } else if (isTomorrow()) {
            text =  getContext().getResources().getString(R.string.tomorrow);
        } else {
            text = formatter.print(date);
        }
        this.setText(text);
    }

    public boolean isToday() {
        if (date == null)
            return false;

        return DateUtils.isToday(date.toDateTime(new LocalTime()).getMillis());
    }

    public boolean isTomorrow() {
        if (date == null)
            return false;

        return DateUtils.isToday(date.minusDays(1).toDateTime(new LocalTime()).getMillis());
    }

    public void setDate(LocalDate date) {
        this.date = date;
        updateView();
    }

    public void setMaxDate(DateTime maxDate) {
        this.maxDate = maxDate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void addOnDateSelectedListener(OnDateSelectedListener onDateSelectedListener) {
        this.onDateSelectedListeners.add(new WeakReference<OnDateSelectedListener>(onDateSelectedListener));
    }

    public void setOnlyFutureDates(boolean onlyFutureDates) {
        this.onlyFutureDates = onlyFutureDates;
    }

    public void setFormatter(DateTimeFormatter formatter) {
        this.formatter = formatter;
    }

    public interface OnDateSelectedListener {
        public void onDateSelected(LocalDate date);
    }
}

