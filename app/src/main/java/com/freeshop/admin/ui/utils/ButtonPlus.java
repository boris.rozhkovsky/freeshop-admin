package com.freeshop.admin.ui.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;


public class ButtonPlus extends Button {

    public ButtonPlus(Context context) {
        super(context);
        init();

    }

    public ButtonPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonPlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface typeface = FontsManager.getTypeface(FontsManager.ROBOTO_MEDIUM);
        setTypeface(typeface);
    }

}
