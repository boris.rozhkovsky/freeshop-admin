package com.freeshop.admin.ui.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.widget.TextView;

import com.freeshop.admin.R;


public class TextViewPlus extends TextView {


    public TextViewPlus(Context context) {
        super(context);
        init();
    }

    public TextViewPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        if (attrs != null)
            readAttributes(context, attrs);
    }

    public TextViewPlus(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        if (attrs != null)
            readAttributes(context, attrs);
    }

    private void init() {
        if (isFocusable()) {
            setAutoLinkMask(Linkify.ALL);
            setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    private void readAttributes(Context ctx, @NonNull AttributeSet attrs) {
        if (this.isInEditMode())
            return;

        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.Font);
        String customFontName = a.getString(R.styleable.Font_font);

        if (TextUtils.isEmpty(customFontName) == false) {
            Typeface typeface = FontsManager.getTypeface(customFontName);
            setTypeface(typeface);
        } else {
            Typeface typeface = FontsManager.getDefaultTypeface();
            setTypeface(typeface);
        }
        a.recycle();
    }

}
