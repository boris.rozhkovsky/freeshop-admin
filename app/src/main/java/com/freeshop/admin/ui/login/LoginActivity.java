package com.freeshop.admin.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.freeshop.admin.R;
import com.freeshop.admin.model.authorization.AuthorizationManager;
import com.freeshop.admin.model.vk.Author;
import com.freeshop.admin.model.vk.list.VkAlbumsList;
import com.freeshop.admin.model.vk.list.VkSystemAlbumsList;
import com.freeshop.admin.ui.BaseActivity;
import com.freeshop.admin.ui.MainActivity;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentFragment(new LoginFragment(), true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                VkAlbumsList.getInstance().update();
                VkSystemAlbumsList.getInstance().update();
                AuthorizationManager.getInstance().setAccessToken(res);
                AuthorizationManager.getInstance().updateCurrentUser();
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            }

            @Override
            public void onError(VKError error) {

                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(R.string.oh)
                        .setMessage(R.string.something_went_wrong_authorization)
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
