package com.freeshop.admin.ui.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.freeshop.admin.R;
import com.freeshop.admin.model.vk.Author;
import com.freeshop.admin.ui.BaseActivity;

public class AuthorView extends LinearLayout implements View.OnClickListener {

    private ImageView avatarImageView;
    private TextView credentialsTextView;

    private Author author;

    public AuthorView(Context context) {
        super(context);
        init(context, null);
    }

    public AuthorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AuthorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(23)
    public AuthorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.author_view, this, true);
        credentialsTextView = (TextView) findViewById(R.id.credentialsTextView);
        avatarImageView = (ImageView) findViewById(R.id.avatarImageView);
        this.setOnClickListener(this);
    }

    public void setAuthor(Author author) {
        this.author = author;
        update();
    }

    private void update() {
        if (author == null) {
            setVisibility(View.GONE);
            return;
        } else {
            setVisibility(View.VISIBLE);
        }

        credentialsTextView.setText(author.getScreenName());
        Glide.with(getContext()).load(author.getAvatarUrl()).asBitmap().centerCrop().into(new BitmapImageViewTarget(avatarImageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                avatarImageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    public void setAvatarVisible(boolean visible) {
        avatarImageView.setVisibility(visible ? VISIBLE : GONE);
    }

    @Override
    public void onClick(View v) {
        if (author != null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vk.com/id" + author.getId()));
            BaseActivity.getCurrentActivity().startActivity(browserIntent);
        }
    }
}
