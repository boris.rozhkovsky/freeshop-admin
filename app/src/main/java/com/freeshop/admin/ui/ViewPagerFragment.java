package com.freeshop.admin.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


import com.freeshop.admin.R;

import java.lang.ref.WeakReference;

public abstract class ViewPagerFragment<T extends Fragment> extends BaseFragment {

    protected ViewPager viewPager;
    protected ViewPagerFragmentAdapter adapter;
    protected CoordinatorLayout root;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_pager_fragment, container, false);
        root = (CoordinatorLayout) view.findViewById(R.id.root);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        adapter = new ViewPagerFragmentAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);

        return view;
    }

    protected abstract T instantiateFragment(int position);

    protected void fragmentCreated(int position) {

    }

    protected void fragmentDestroyed(int position) {

    }

    protected abstract int getCount();

    protected int getCurrentPosition() {
        if (viewPager == null) {
            return 0;
        }
        return viewPager.getCurrentItem();
    }

    protected String getTabTitle(int position) {
        return null;
    }

    protected void setCurrentPosition(int newPosition, boolean animate) {
        if (newPosition < 0 || newPosition >= getCount())
            return;

        viewPager.setCurrentItem(newPosition, animate);
    }

    protected Object getFragmentAt(int position) {
        return adapter.getRegisteredFragment(position);
    }

    @Override
    public void onPause() {
        b = loadBitmapFromView(getView());
        super.onPause();
    }

    private Bitmap b = null;

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(),
                v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getWidth(),
                v.getHeight());
        v.draw(c);
        return b;
    }

    @Override
    public void onDestroyView() {
        //Hack to animate viewPager fragments with parent`s Animation
        BitmapDrawable bd = new BitmapDrawable(b);
        getView().findViewById(R.id.viewPager).setBackgroundDrawable(bd);
        b = null;
        super.onDestroyView();
    }

    public  class ViewPagerFragmentAdapter extends FragmentPagerAdapter {
        SparseArray<WeakReference<Object>> registeredFragments = new SparseArray<>();

        public ViewPagerFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ViewPagerFragment.this.instantiateFragment(position);
        }

        @Override
        public int getCount() {
            return ViewPagerFragment.this.getCount();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object fragment = super.instantiateItem(container, position);
            registeredFragments.put(position, new WeakReference<>(fragment));
            ViewPagerFragment.this.fragmentCreated(position);
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (ViewPagerFragment.this.getTabTitle(position) != null) {
                return ViewPagerFragment.this.getTabTitle(position);
            }
            return super.getPageTitle(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ViewPagerFragment.this.fragmentDestroyed(position);
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public
        @Nullable
        Object getRegisteredFragment(int position) {
            WeakReference registeredFragmentReference = registeredFragments.get(position);
            if (registeredFragmentReference == null) return null;
            if (registeredFragmentReference.get() == null)
                registeredFragments.remove(position);
            return registeredFragmentReference.get();
        }
    }

}
