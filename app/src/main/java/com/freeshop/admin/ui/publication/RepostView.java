package com.freeshop.admin.ui.publication;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.freeshop.admin.FreeshopAdminApplication;
import com.freeshop.admin.R;
import com.freeshop.admin.model.vk.Post;
import com.freeshop.admin.ui.attachments.AttachmentsAdapter;
import com.freeshop.admin.ui.utils.AuthorView;
import com.freeshop.admin.ui.utils.TextViewPlus;

public class RepostView extends LinearLayout {


    private TextViewPlus textTextView;
    private AuthorView fromView;
    private RecyclerView attachmentsRecyclerView;
    private FrameLayout attachmentContainer;

    public RepostView(Context context) {
        super(context);
        init(context);
    }

    public RepostView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RepostView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(23)
    public RepostView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.repost_view, this, true);
        setOrientation(VERTICAL);
        textTextView = (TextViewPlus) findViewById(R.id.textTextView_repost);
        fromView = (AuthorView) findViewById(R.id.from_view_repost);
        attachmentsRecyclerView = (RecyclerView) findViewById(R.id.attachmentsRecyclerView_repost);
        attachmentContainer = (FrameLayout) findViewById(R.id.attachmentsContainer_repost);

        attachmentsRecyclerView.setHorizontalScrollBarEnabled(true);
        attachmentsRecyclerView.setHorizontalFadingEdgeEnabled(true);

    }

    public void initWithPost(Post post){
        if (post == null) {
            fromView.setAuthor(null);
            return;
        }

        fromView.setAuthor(post.getFrom());

        textTextView.setText(post.getText());

        if (post.getAttachments() != null && post.getAttachments().size() != 0) {
            attachmentContainer.setVisibility(View.VISIBLE);
            AttachmentsAdapter attachmentsAdapter = new AttachmentsAdapter(getContext(), post.getAttachments(), AttachmentsAdapter.AdapterState.VIEW);
            attachmentsRecyclerView.setLayoutManager(new LinearLayoutManager(FreeshopAdminApplication.getAppContext(), LinearLayoutManager.HORIZONTAL, false));
            attachmentsRecyclerView.setAdapter(attachmentsAdapter);
            attachmentsAdapter.notifyDataSetChanged();
        } else {
            attachmentContainer.setVisibility(View.GONE);
        }
    }
}
