package com.freeshop.admin.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.freeshop.admin.R;
import com.freeshop.admin.ui.BaseFragment;
import com.vk.sdk.VKSdk;

public class LoginFragment extends BaseFragment {

    private Button loginButton;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);
        toolbar.setNavigationIcon(null);
        loginButton = (Button) view.findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VKSdk.login(getActivity(), new String[]{"friends", "wall", "groups", "photos"});
            }
        });
        return view;
    }

    @Override
    public String getTitle() {
        return getAppContext().getString(R.string.login_title);
    }

}
