package com.freeshop.admin.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.freeshop.admin.R;
import com.freeshop.admin.model.UpdatableList;

import java.util.List;


public abstract class UpdatableListFragment<T extends UpdatableList> extends UpdatableFragment<T> {

    protected RecyclerView recyclerView;

    @Override
    protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    protected List<T> getItems(){
        return updatableModel.getItems();
    }
}

