package com.freeshop.admin.ui;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.freeshop.admin.FreeshopAdminApplication;

public class BaseFragment extends Fragment {

    protected Toolbar toolbar;
    protected BaseActivity activity;

    public String getTitle() {
        return null;
    }

    protected TabLayout getTabLayout() {
        return null;
    }

    protected Context getAppContext() {
        return FreeshopAdminApplication.getAppContext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() != null) {
            activity = (BaseActivity) getActivity();
        } else {
            activity = BaseActivity.getCurrentActivity();
        }
        toolbar = activity.getToolbar();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
