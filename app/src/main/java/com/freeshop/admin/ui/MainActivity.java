package com.freeshop.admin.ui;

import android.os.Bundle;

import com.freeshop.admin.ui.news.MainViewPagerFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentFragment(new MainViewPagerFragment(), true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
