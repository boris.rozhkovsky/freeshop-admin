package com.freeshop.admin.ui.news;

import com.freeshop.admin.R;
import com.freeshop.admin.model.UpdatableModel;
import com.freeshop.admin.model.vk.list.NewsFeedPostList;
import com.freeshop.admin.ui.BaseActivity;
import com.freeshop.admin.ui.utils.FontsManager;
import com.freeshop.admin.ui.utils.TextViewPlus;

public class NewsFeedFragment extends VkPostsListFragment {

    private OnFragmentResumeListener onFragmentResumeListener;

    public NewsFeedFragment() {

    }

    @Override
    protected UpdatableModel getUpdatableModel() {
        return NewsFeedPostList.getInstance();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (onFragmentResumeListener != null) {
            onFragmentResumeListener.onFragmentResumed();
        }
    }

    public void setOnFragmentResumeListener(OnFragmentResumeListener onFragmentResumeListener) {
        this.onFragmentResumeListener = onFragmentResumeListener;
    }

    public interface OnFragmentResumeListener {
        void onFragmentResumed();
    }
}
