package com.freeshop.admin.ui.publication;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.freeshop.admin.R;
import com.freeshop.admin.api.tasks.BanUserTask;
import com.freeshop.admin.api.tasks.VkAsyncTask;
import com.freeshop.admin.model.vk.User;
import com.freeshop.admin.ui.utils.DatePicker;

import org.joda.time.DateTime;

public class BanDialog extends AlertDialog {

    private User userToBan;
    private int reason = 0;

    private TextView banTextTextView;
    private TextView untilTextView;
    private RadioButton otherRadioButton;
    private RadioButton spamRadioButton;
    private RadioButton userInsultRadioButton;
    private RadioButton obsceneExpressionRadioButton;
    private RadioButton offTopicRadioButton;
    private EditText commentEditText;
    private CheckBox showCommentCheckbox;
    private CheckBox constantBanCheckbox;
    private DatePicker endDatePicker;
    private Button okButton;
    private Button cancelButton;
    private BanListener banListener;

    public BanDialog(User user, Context context, BanListener banListener) {
        super(context);
        this.userToBan = user;
        this.banListener = banListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.ban_user_dialog);
        banTextTextView = (TextView) findViewById(R.id.banTextView);
        otherRadioButton = (RadioButton) findViewById(R.id.otherRadioButton);
        spamRadioButton = (RadioButton) findViewById(R.id.spamRadioButton);
        userInsultRadioButton = (RadioButton) findViewById(R.id.userInsultRadioButton);
        obsceneExpressionRadioButton = (RadioButton) findViewById(R.id.obsceneExpressionRadioButton);
        offTopicRadioButton = (RadioButton) findViewById(R.id.offTopicRadioButton);
        commentEditText = (EditText) findViewById(R.id.comment);
        showCommentCheckbox = (CheckBox) findViewById(R.id.showCommentCheckbox);
        constantBanCheckbox = (CheckBox) findViewById(R.id.constantBanCheckbox);
        endDatePicker = (DatePicker) findViewById(R.id.banEndDate);
        untilTextView = (TextView) findViewById(R.id.untilTextView);
        okButton = (Button) findViewById(R.id.ok);
        cancelButton = (Button) findViewById(R.id.cancel);

        endDatePicker.setOnlyFutureDates(true);
        endDatePicker.setMaxDate(DateTime.now().plusYears(1));
        banTextTextView.setText(String.format(getContext().getResources().getString(R.string.sure_to_ban), userToBan.getScreenName()));

        otherRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reason = 0;
                }
            }
        });

        spamRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reason = 1;
                }
            }
        });

        userInsultRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reason = 2;
                }
            }
        });

        obsceneExpressionRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reason = 3;
                }
            }
        });

        offTopicRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    reason = 4;
                }
            }
        });

        constantBanCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    endDatePicker.setVisibility(View.GONE);
                    untilTextView.setVisibility(View.GONE);
                } else {
                    untilTextView.setVisibility(View.VISIBLE);
                    endDatePicker.setVisibility(View.VISIBLE);
                    endDatePicker.setDate(DateTime.now().plusDays(3).toLocalDate());
                }
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BanUserTask banUserTask = new BanUserTask(getContext(),
                        userToBan,
                        commentEditText.getText().toString(),
                        endDatePicker.getDate(),
                        reason,
                        showCommentCheckbox.isChecked(),
                        constantBanCheckbox.isChecked(),
                        new VkAsyncTask.VkAsyncTaskListener() {
                            @Override
                            public void onSuccess(Object data) {
                                if (banListener != null) {
                                    banListener.onBanSuccess();
                                }
                            }

                            @Override
                            public void onError() {

                            }
                        }

                );
                banUserTask.execute();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                cancel();
                                            }
                                        }

        );
    }

    public interface BanListener {
        void onBanSuccess();
    }

}


