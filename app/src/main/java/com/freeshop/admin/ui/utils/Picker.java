package com.freeshop.admin.ui.utils;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.freeshop.admin.R;
import com.freeshop.admin.utils.Utils;

import java.util.Calendar;

public abstract class Picker extends AppCompatEditText {

    public Picker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public Picker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    protected abstract void onClick();

    protected void init(Context context, AttributeSet attrs) {

        setSingleLine(true);
        setFocusable(false);
        setFocusableInTouchMode(false);
        setHintTextColor(context.getResources().getColor(R.color.greyish));

        this.setOnTouchListener(new OnTouchListener() {

            private static final int MAX_CLICK_DURATION = 200;
            private long startClickTime;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if (clickDuration < MAX_CLICK_DURATION) {
                            Utils.hideKeyboard(v);
                            onClick();
                        }
                    }
                }
                return true;
            }
        });
    }
}
