package com.freeshop.admin.ui.attachments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.freeshop.admin.FreeshopAdminApplication;
import com.freeshop.admin.R;
import com.freeshop.admin.api.tasks.DeletePostTask;
import com.freeshop.admin.api.tasks.VkAsyncTask;
import com.freeshop.admin.model.vk.attachments.Attachment;

import java.util.ArrayList;
import java.util.List;

public class AttachmentsAdapter extends RecyclerView.Adapter<AttachmentsAdapter.AttachmentViewHolder> {

    private AdapterState adapterState;
    private Context context;
    private List<Attachment> attachments = new ArrayList<>();

    public AttachmentsAdapter(Context context, List<Attachment> attachments, AdapterState state) {
        this.attachments = attachments;
        this.adapterState = state;
        this.context = context;
    }

    @Override
    public AttachmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_photo_cell, parent, false);
        AttachmentViewHolder postViewHolder = new AttachmentViewHolder(v);
        return postViewHolder;
    }

    @Override
    public void onBindViewHolder(AttachmentViewHolder holder, int position) {
        Attachment attachment = attachments.get(position);
        String previewUrl = attachment.getPreviewImageUrl();
        holder.attachment = attachment;

        switch (adapterState) {
            case EDIT:
                holder.deleteButton.setVisibility(View.VISIBLE);
                break;
            case VIEW:
                holder.deleteButton.setVisibility(View.GONE);
                break;
        }

        Glide.with(FreeshopAdminApplication.getAppContext())
                .load(previewUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return attachments.size();
    }

    public void setAdapterState(AdapterState adapterState) {
        this.adapterState = adapterState;
    }

    class AttachmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Attachment attachment;
        ImageView imageView;
        ImageButton deleteButton;

        AttachmentViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            deleteButton = (ImageButton) itemView.findViewById(R.id.deleteButton);
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDeleteAttachmentsDialog(attachment);
                }
            });
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), FullScreenAttachmentActivity.class);
            intent.putExtra(FullScreenAttachmentActivity.ATTACHMENT_URL, attachment.getImageUrl());
            v.getContext().startActivity(intent);
        }
    }

    private void showDeleteAttachmentsDialog(final Attachment attachment) {

        new AlertDialog.Builder(context)
                .setTitle(R.string.delete_attachment_dialog_title)
                .setMessage(R.string.delete_attachment_dialog_message)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AttachmentsAdapter.this.attachments.remove(attachment);
                        AttachmentsAdapter.this.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    public enum AdapterState {
        EDIT,
        VIEW
    }
}
