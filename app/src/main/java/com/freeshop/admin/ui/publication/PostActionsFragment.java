package com.freeshop.admin.ui.publication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.freeshop.admin.Config;
import com.freeshop.admin.FreeshopAdminApplication;
import com.freeshop.admin.R;
import com.freeshop.admin.api.tasks.DeletePostTask;
import com.freeshop.admin.api.tasks.EditPostTask;
import com.freeshop.admin.api.tasks.PublishNewPostTask;
import com.freeshop.admin.api.tasks.PublishPostTask;
import com.freeshop.admin.api.tasks.SendToTelegramTask;
import com.freeshop.admin.api.tasks.UploadPhotoTask;
import com.freeshop.admin.api.tasks.VkAsyncTask;
import com.freeshop.admin.model.telegram.BotList;
import com.freeshop.admin.model.telegram.Channel;
import com.freeshop.admin.model.vk.Post;
import com.freeshop.admin.model.vk.User;
import com.freeshop.admin.model.vk.attachments.Photo;
import com.freeshop.admin.model.vk.list.NewsFeedPostList;
import com.freeshop.admin.model.vk.list.SuggestionsPostList;
import com.freeshop.admin.telegram.ChannelPickerDialog;
import com.freeshop.admin.ui.BaseActivity;
import com.freeshop.admin.ui.BaseFragment;
import com.freeshop.admin.ui.attachments.AttachmentsAdapter;
import com.freeshop.admin.ui.utils.AuthorView;
import com.vk.sdk.api.model.VKPhotoArray;

import java.io.File;

public class PostActionsFragment extends BaseFragment implements ChannelPickerDialog.OnChannelSelectedListener {

    private EditText textTextView;
    private AuthorView fromView;
    private AuthorView signerView;
    private RecyclerView attachmentsRecyclerView;
    private FrameLayout attachmentContainer;
    private CheckBox signedCheckbox;
    private Button postButton;
    private Button postSuggestedButton;
    private Button dontPostButton;
    private Button saveEditsButton;
    private Button addPhotoButton;
    private ActionType actionType;
    private LinearLayout repostContainer;
    private RepostView repostView;
    private AttachmentsAdapter attachmentsAdapter;
    private CheckBox banCheckbox;
    private RadioButton telegramRadioButton;
    private RadioButton vkRadioButton;
    private RadioButton telegramVkRadioButton;
    private LinearLayout postSuggestedContainer;

    private Post post;

    private static String POST_ID_EXTRA = "POST_ID_EXTRA";
    private int PICK_IMAGE_REQUEST = 1;

    public static PostActionsFragment getInstance(long postId) {
        PostActionsFragment fragment = new PostActionsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(POST_ID_EXTRA, postId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(POST_ID_EXTRA)) {
                long postId = args.getLong(POST_ID_EXTRA);

                post = NewsFeedPostList.getInstance().getPost(postId);
                if (post == null) {
                    post = SuggestionsPostList.getInstance().getPost(postId);
                }

                if (post != null) {
                    actionType = post.getPostType() == Post.PostType.SUGGEST ? ActionType.POST_SUGGESTED : ActionType.EDIT_EXISTED;
                } else {
                    Log.i("","Cannot find post, looks like you want new post");
                }
            }
        }

        if (post == null) {
            actionType = ActionType.POST_NEW;
            post = new Post(-1, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_fragment, null);

        textTextView = (EditText) view.findViewById(R.id.textTextView);
        fromView = (AuthorView) view.findViewById(R.id.from_view);
        signerView = (AuthorView) view.findViewById(R.id.signer_view);
        signerView.setAvatarVisible(false);
        attachmentsRecyclerView = (RecyclerView) view.findViewById(R.id.attachmentsRecyclerView);
        attachmentContainer = (FrameLayout) view.findViewById(R.id.attachmentsContainer);
        postButton = (Button) view.findViewById(R.id.postButton);
        saveEditsButton = (Button) view.findViewById(R.id.saveButton);
        postSuggestedButton = (Button) view.findViewById(R.id.postSuggestedButton);
        dontPostButton = (Button) view.findViewById(R.id.dontPostButton);
        signedCheckbox = (CheckBox) view.findViewById(R.id.checkboxSign);
        repostContainer = (LinearLayout) view.findViewById(R.id.repostContainer);
        repostView = (RepostView) view.findViewById(R.id.repostView);
        addPhotoButton = (Button) view.findViewById(R.id.addPhotoButton);
        telegramRadioButton = (RadioButton) view.findViewById(R.id.postTelegram);
        vkRadioButton = (RadioButton) view.findViewById(R.id.postVk);
        telegramVkRadioButton = (RadioButton) view.findViewById(R.id.postTelegramVk);
        postSuggestedContainer = (LinearLayout) view.findViewById(R.id.postSuggestedContainer);
        banCheckbox = (CheckBox) view.findViewById(R.id.banCheckbox);
        attachmentsRecyclerView.setHorizontalScrollBarEnabled(true);
        attachmentsRecyclerView.setHorizontalFadingEdgeEnabled(true);

        initWithPost(post);
        updateVisibility();
        updateTitle();

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postNew();
            }
        });

        postSuggestedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postSuggested();
            }
        });

        saveEditsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editExisted();
            }
        });

        dontPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dontPublish(banCheckbox.isChecked());
            }
        });

        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        return view;
    }


    private void initWithPost(Post post) {
        signerView.setAuthor(null);
        fromView.setAuthor(null);

        attachmentsAdapter = new AttachmentsAdapter(getContext(), post.getAttachments(), AttachmentsAdapter.AdapterState.EDIT);
        attachmentsRecyclerView.setLayoutManager(new LinearLayoutManager(FreeshopAdminApplication.getAppContext(), LinearLayoutManager.HORIZONTAL, false));
        attachmentsRecyclerView.setAdapter(attachmentsAdapter);
        attachmentsAdapter.notifyDataSetChanged();

        fromView.setAuthor(post.getFrom());
        textTextView.setText(post.getText());
        signerView.setAuthor(post.getSigner());
        if (actionType.equals(ActionType.POST_SUGGESTED) || actionType.equals(ActionType.POST_NEW)) {
            signedCheckbox.setChecked(true);
        } else {
            signedCheckbox.setChecked(post.getSigner() != null);
        }

        if (post.getCopyHistory() != null && post.getCopyHistory().size() > 0) {
            repostView.setVisibility(View.VISIBLE);
            repostContainer.setVisibility(View.VISIBLE);
            repostView.initWithPost(post.getCopyHistory().get(0));
        } else {
            repostContainer.setVisibility(View.GONE);
            repostView.setVisibility(View.GONE);
        }

        if (post.getAttachments() != null && post.getAttachments().size() != 0) {
            attachmentContainer.setVisibility(View.VISIBLE);
        } else {
            attachmentContainer.setVisibility(View.GONE);
        }

        signedCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                refreshSignerViewOpacity();
            }
        });
        refreshSignerViewOpacity();
    }

    private void updateVisibility() {
        switch (actionType) {
            case EDIT_EXISTED:
                saveEditsButton.setVisibility(View.VISIBLE);
                postButton.setVisibility(View.GONE);
                postSuggestedContainer.setVisibility(View.GONE);
                break;
            case POST_SUGGESTED:
                repostContainer.setVisibility(View.GONE);
                saveEditsButton.setVisibility(View.GONE);
                postButton.setVisibility(View.GONE);
                postSuggestedContainer.setVisibility(View.VISIBLE);
                break;
            case POST_NEW:
                repostContainer.setVisibility(View.GONE);
                saveEditsButton.setVisibility(View.GONE);
                postButton.setVisibility(View.VISIBLE);
                postSuggestedContainer.setVisibility(View.GONE);
                break;
        }
    }

    private void updateTitle() {
        if (!(getActivity() instanceof BaseActivity))
            return;

        BaseActivity currentActivity = (BaseActivity) getActivity();

        switch (actionType) {
            case EDIT_EXISTED:
                currentActivity.setTitle(getContext().getResources().getString(R.string.edit_title));
                break;
            case POST_SUGGESTED:
                currentActivity.setTitle(getContext().getResources().getString(R.string.post_suggestion_title));
                break;
            case POST_NEW:
                currentActivity.setTitle(getContext().getResources().getString(R.string.new_post_title));
                break;
        }
    }

    @Override
    public String getTitle() {
        return null;
    }

    private void refreshSignerViewOpacity() {
        if (signedCheckbox.isChecked()) {
            signerView.setAlpha(1f);
        } else {
            signerView.setAlpha(0.3f);
        }
    }

    private void postSuggested() {
        boolean signed = signedCheckbox.isChecked();

        final PublicationType publicationType = getPublicationType();

        post.setText(textTextView.getText().toString());
        if (publicationType.equals(PublicationType.TELEGRAM)) {
            DeletePostTask deletePostTask = new DeletePostTask(getActivity(), post, new VkAsyncTask.VkAsyncTaskListener() {
                @Override
                public void onSuccess(Object data) {
                    ChannelPickerDialog channelPickerDialog = new ChannelPickerDialog(getActivity(), false, PostActionsFragment.this);
                    channelPickerDialog.show();
                }

                @Override
                public void onError() {

                }
            });
            deletePostTask.execute();
            return;

        } else if (publicationType.equals(PublicationType.VK) || publicationType.equals(PublicationType.TELEGRAM_VK)) {
            PublishPostTask publishPostTask = new PublishPostTask(getActivity(), post, signed, new VkAsyncTask.VkAsyncTaskListener() {
                @Override
                public void onSuccess(Object data) {
                    if (getActivity() != null) {
                        if (publicationType.equals(PublicationType.VK)) {
                            if (getActivity() != null) {
                                getActivity().finish();
                            }
                        } else {
                            ChannelPickerDialog channelPickerDialog = new ChannelPickerDialog(PostActionsFragment.this.getActivity(), PostActionsFragment.this);
                            channelPickerDialog.show();
                        }
                    }
                }

                @Override
                public void onError() {

                }
            });
            publishPostTask.execute();
        }


    }

    private void editExisted() {
        boolean signed = signedCheckbox.isChecked();
        post.setText(textTextView.getText().toString());
        EditPostTask editPostTask = new EditPostTask(getActivity(), post, signed, new VkAsyncTask.VkAsyncTaskListener() {
            @Override
            public void onSuccess(Object data) {
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }

            @Override
            public void onError() {

            }
        });
        editPostTask.execute();

    }

    private void postNew() {
        boolean signed = signedCheckbox.isChecked();

        PublishNewPostTask publishNewPostTask = new PublishNewPostTask(getActivity(),
                Config.currentGroup.getGroupId(),
                post.getAttachments(),
                textTextView.getText().toString(),
                signed,
                new VkAsyncTask.VkAsyncTaskListener() {
                    @Override
                    public void onSuccess(Object data) {
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
        publishNewPostTask.execute();
    }

    private void dontPublish(final boolean ban) {

        final DeletePostTask deletePostTask = new DeletePostTask(getActivity(), post, new VkAsyncTask.VkAsyncTaskListener() {
            @Override
            public void onSuccess(Object data) {
                if (ban) {
                    BanDialog banDialog = new BanDialog((User) post.getFrom(), getActivity(), new BanDialog.BanListener() {
                        @Override
                        public void onBanSuccess() {
                            if (getActivity() != null) {
                                getActivity().finish();
                            }
                        }
                    });
                    banDialog.show();
                } else {
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onError() {

            }
        });

        deletePostTask.execute();
    }


    public void onImageSelected(final Uri uri) {

        UploadPhotoTask uploadTask = new UploadPhotoTask(getActivity(), new File(uri.getPath()), uri, new VkAsyncTask.VkAsyncTaskListener() {
            @Override
            public void onSuccess(Object data) {
                if (data instanceof VKPhotoArray) {
                    post.addAttachment(new Photo(((VKPhotoArray) data).get(0)));
                }
                if (attachmentsAdapter.getItemCount() != 0) {
                    attachmentsRecyclerView.setVisibility(View.VISIBLE);
                    attachmentContainer.setVisibility(View.VISIBLE);
                }
                attachmentsRecyclerView.setAdapter(attachmentsAdapter);
                attachmentsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError() {

            }
        });
        uploadTask.execute();
    }

    @Override
    public void onChannelSelected(Channel item, boolean silently) {
        SendToTelegramTask sendToTelegramTask = new SendToTelegramTask(getActivity(), BotList.getBot(), item, post.toTelegramString(), silently, new SendToTelegramTask.TelegramTaskListener() {
            @Override
            public void onSuccess(Object data) {
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }

            @Override
            public void onError() {

            }
        });
        sendToTelegramTask.execute();
    }

    private PublicationType getPublicationType() {
        if (telegramRadioButton.isChecked()) {
            return PublicationType.TELEGRAM;
        } else if (vkRadioButton.isChecked()) {
            return PublicationType.VK;
        } else if (telegramVkRadioButton.isChecked()) {
            return PublicationType.TELEGRAM_VK;
        }
        return PublicationType.TELEGRAM;
    }

    private enum ActionType {
        EDIT_EXISTED,
        POST_NEW,
        POST_SUGGESTED
    }

    private enum PublicationType {
        TELEGRAM,
        VK,
        TELEGRAM_VK
    }

}
