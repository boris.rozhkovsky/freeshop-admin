package com.freeshop.admin.ui.utils;

import android.graphics.Typeface;

import com.freeshop.admin.FreeshopAdminApplication;

import java.util.ArrayList;
import java.util.HashMap;

public class FontsManager {

    public static class CustomFont {
        String name;
        String font;

        public CustomFont(String name, String font) {
            this.name = name;
            this.font = font;
        }
    }

    public static final CustomFont ROBOTO_REGULAR = new CustomFont("roboto-regular", "fonts/Roboto-Regular.ttf");
    public static final CustomFont ROBOTO_MEDIUM = new CustomFont("roboto-medium", "fonts/Roboto-Medium.ttf");

    private static ArrayList<CustomFont> customFonts = new ArrayList<CustomFont>();
    private static HashMap<CustomFont, Typeface> cachedFonts = new HashMap<CustomFont, Typeface>();

    static {
        customFonts.add(ROBOTO_REGULAR);
        customFonts.add(ROBOTO_MEDIUM);
    }

    public static Typeface getTypeface(String typefaceName) {
        CustomFont font = null;
        for (int i = 0; i < customFonts.size(); ++i) {
            if (customFonts.get(i).name.equalsIgnoreCase(typefaceName)) {
                font = customFonts.get(i);
                break;
            }
        }
        if (font == null) {
            throw new RuntimeException("Font not found: " + typefaceName);
        }
        return getTypeface(font);
    }

    public static Typeface getTypeface(CustomFont typefaceName) {
        if (!cachedFonts.containsKey(typefaceName))
            cachedFonts.put(typefaceName, Typeface.createFromAsset(FreeshopAdminApplication.getInstance().getAssets(), typefaceName.font));
        return cachedFonts.get(typefaceName);
    }

    public static Typeface getDefaultTypeface() {
        return getTypeface(ROBOTO_REGULAR);
    }
}
