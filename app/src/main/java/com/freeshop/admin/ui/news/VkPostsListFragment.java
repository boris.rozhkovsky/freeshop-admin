package com.freeshop.admin.ui.news;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freeshop.admin.FreeshopAdminApplication;
import com.freeshop.admin.R;
import com.freeshop.admin.api.tasks.DeletePostTask;
import com.freeshop.admin.api.tasks.SendToTelegramTask;
import com.freeshop.admin.api.tasks.VkAsyncTask;
import com.freeshop.admin.model.UpdatableModel;
import com.freeshop.admin.model.telegram.BotList;
import com.freeshop.admin.model.telegram.Channel;
import com.freeshop.admin.model.vk.Post;
import com.freeshop.admin.telegram.ChannelPickerDialog;
import com.freeshop.admin.ui.UpdatableListFragment;
import com.freeshop.admin.ui.attachments.AttachmentsAdapter;
import com.freeshop.admin.ui.publication.PostActionsActivity;
import com.freeshop.admin.ui.publication.RepostView;
import com.freeshop.admin.ui.utils.AuthorView;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

public abstract class VkPostsListFragment extends UpdatableListFragment {

    private VkPostsAdapter adapter;

    @Override
    protected View getContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.getContentView(inflater, container, savedInstanceState);
        adapter = new VkPostsAdapter();
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    protected void updateView(UpdatableModel updatableModel) {
        adapter.setPosts(getItems());
        adapter.notifyDataSetChanged();
    }

    private class VkPostsAdapter extends RecyclerView.Adapter<VkPostsAdapter.PostViewHolder> {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(" HH:mm dd MMMM yyyy");

        private List<Post> posts;

        @Override
        public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_cell, parent, false);
            VkPostsAdapter.PostViewHolder postViewHolder = new VkPostsAdapter.PostViewHolder(v);
            return postViewHolder;
        }

        @Override
        public void onBindViewHolder(PostViewHolder holder, final int position) {
            Post post = posts.get(position);
            holder.fromView.setAuthor(post.getFrom());

            if (post.getCopyHistory() != null && post.getCopyHistory().size() > 0) {
                holder.repostView.setVisibility(View.VISIBLE);
                holder.repostContainer.setVisibility(View.VISIBLE);
                holder.repostView.initWithPost(post.getCopyHistory().get(0));
            } else {
                holder.repostContainer.setVisibility(View.GONE);
                holder.repostView.setVisibility(View.GONE);
            }

            holder.deleteButton.setVisibility(post.getPostType() == Post.PostType.SUGGEST ? View.GONE : View.VISIBLE);
            holder.textTextView.setText(post.getText());
            holder.signerView.setAvatarVisible(false);
            holder.signerView.setAuthor(post.getSigner());
            holder.dateTimeTextView.setText(formatter.print(post.getTime()));

            switch (post.getPostType()) {
                case SUGGEST:
                    holder.deleteButton.setVisibility(View.GONE);
                    holder.actionsButton.setVisibility(View.VISIBLE);
                    holder.editButton.setVisibility(View.GONE);
                    holder.sendToTelegramButton.setVisibility(View.GONE);
                    break;
                case POST:
                    holder.deleteButton.setVisibility(post.isCanDelete() ? View.VISIBLE : View.GONE);
                    holder.actionsButton.setVisibility(View.GONE);
                    holder.editButton.setVisibility(post.isCanEdit() ? View.VISIBLE : View.GONE);
                    holder.sendToTelegramButton.setVisibility(View.VISIBLE);
                    break;
            }

            if (post.getAttachments() != null && post.getAttachments().size() != 0) {
                holder.attachmentContainer.setVisibility(View.VISIBLE);
                AttachmentsAdapter attachmentsAdapter = new AttachmentsAdapter(getContext(), post.getAttachments(), AttachmentsAdapter.AdapterState.VIEW);
                holder.attachmentsRecyclerView.setLayoutManager(new LinearLayoutManager(FreeshopAdminApplication.getAppContext(), LinearLayoutManager.HORIZONTAL, false));
                holder.attachmentsRecyclerView.setAdapter(attachmentsAdapter);
                attachmentsAdapter.notifyDataSetChanged();
            } else {
                holder.attachmentContainer.setVisibility(View.GONE);
            }

            holder.actionsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Post post = posts.get(position);
                    Intent i = new Intent(VkPostsListFragment.this.getActivity(), PostActionsActivity.class);
                    i.putExtra(PostActionsActivity.POST_ID_EXTRA, post.getId());
                    startActivity(i);
                }
            });

            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Post post = posts.get(position);
                    showDeletePostDialog(post);
                }
            });

            holder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Post post = posts.get(position);
                    Intent i = new Intent(VkPostsListFragment.this.getActivity(), PostActionsActivity.class);
                    i.putExtra(PostActionsActivity.POST_ID_EXTRA, post.getId());
                    startActivity(i);
                }
            });

            holder.sendToTelegramButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Post post = posts.get(position);
                    showsSendToTelegramDialog(post);
                }
            });
        }

        @Override
        public int getItemCount() {
            return posts.size();
        }

        public void setPosts(List<Post> posts) {
            this.posts = posts;
        }

        public class PostViewHolder extends RecyclerView.ViewHolder {
            CardView cv;
            TextView textTextView;
            TextView dateTimeTextView;
            AuthorView fromView;
            RepostView repostView;
            LinearLayout repostContainer;
            AuthorView signerView;
            RecyclerView attachmentsRecyclerView;
            FrameLayout attachmentContainer;
            Button actionsButton;
            ImageButton deleteButton;
            ImageButton editButton;
            ImageButton sendToTelegramButton;

            PostViewHolder(View itemView) {
                super(itemView);
                cv = (CardView) itemView.findViewById(R.id.cardView);
                textTextView = (TextView) itemView.findViewById(R.id.textTextView);
                fromView = (AuthorView) itemView.findViewById(R.id.from_view);
                repostView = (RepostView) itemView.findViewById(R.id.repostView);
                repostContainer = (LinearLayout) itemView.findViewById(R.id.repostContainer);
                signerView = (AuthorView) itemView.findViewById(R.id.signer_view);
                dateTimeTextView = (TextView) itemView.findViewById(R.id.dateTimeTextView);
                attachmentsRecyclerView = (RecyclerView) itemView.findViewById(R.id.attachmentsRecyclerView);
                attachmentContainer = (FrameLayout) itemView.findViewById(R.id.attachmentsContainer);
                actionsButton = (Button) itemView.findViewById(R.id.actionsButton);
                deleteButton = (ImageButton) itemView.findViewById(R.id.deletePostButton);
                editButton = (ImageButton) itemView.findViewById(R.id.editPostButton);
                sendToTelegramButton = (ImageButton) itemView.findViewById(R.id.telegramButton);
                attachmentsRecyclerView.setHorizontalScrollBarEnabled(true);
                attachmentsRecyclerView.setHorizontalFadingEdgeEnabled(true);
            }
        }
    }

    public void showDeletePostDialog(final Post post) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.delete_post_dialog_title)
                .setMessage(R.string.delete_post_dialog_message)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DeletePostTask deletePostTask = new DeletePostTask(getContext(), post, new VkAsyncTask.VkAsyncTaskListener() {
                            @Override
                            public void onSuccess(Object data) {
                                updateModel();
                            }

                            @Override
                            public void onError() {

                            }
                        });
                        deletePostTask.execute();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    public void showsSendToTelegramDialog(final Post post){
        ChannelPickerDialog channelPickerDialog = new ChannelPickerDialog(getContext(), new ChannelPickerDialog.OnChannelSelectedListener() {
            @Override
            public void onChannelSelected(Channel item, boolean silently) {
                SendToTelegramTask task = new SendToTelegramTask(getActivity(), BotList.getBot(), item, post.toTelegramString(), silently, null);
                task.execute();
            }
        });
        channelPickerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        channelPickerDialog.show();
    }

    public void addOnScrollListener(RecyclerView.OnScrollListener listener) {
        recyclerView.addOnScrollListener(listener);
    }

}
