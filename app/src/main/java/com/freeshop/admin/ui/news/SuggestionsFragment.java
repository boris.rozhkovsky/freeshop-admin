package com.freeshop.admin.ui.news;

import com.freeshop.admin.model.UpdatableModel;
import com.freeshop.admin.model.vk.list.SuggestionsPostList;

public class SuggestionsFragment extends VkPostsListFragment {

    @Override
    protected UpdatableModel getUpdatableModel() {
       return SuggestionsPostList.getInstance();
    }
}
