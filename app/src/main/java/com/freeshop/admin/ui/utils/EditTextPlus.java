package com.freeshop.admin.ui.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;

import com.freeshop.admin.R;


public class EditTextPlus extends AppCompatEditText {

    public EditTextPlus(Context context) {
        super(context);
        init(context);
    }

    public EditTextPlus(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null)
            readAttributes(context, attrs);
        init(context);
    }

    public EditTextPlus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null)
            readAttributes(context, attrs);
        init(context);
    }

    private void init(Context context){
        this.setHintTextColor(context.getResources().getColor(R.color.greyish));
    }

    private void readAttributes(Context ctx, @NonNull AttributeSet attrs) {
        if (this.isInEditMode())
            return;

        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.Font);
        String customFontName = a.getString(R.styleable.Font_font);

        if (TextUtils.isEmpty(customFontName) == false) {
            Typeface typeface = FontsManager.getTypeface(customFontName);
            setTypeface(typeface);
        } else {
            Typeface typeface = FontsManager.getDefaultTypeface();
            setTypeface(typeface);
        }
        a.recycle();
    }
}
