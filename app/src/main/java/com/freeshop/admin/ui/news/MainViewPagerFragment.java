package com.freeshop.admin.ui.news;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.freeshop.admin.R;
import com.freeshop.admin.model.UpdatableModel;
import com.freeshop.admin.model.vk.list.UpdatableVkPostList;
import com.freeshop.admin.ui.BaseFragment;
import com.freeshop.admin.ui.ViewPagerFragment;
import com.freeshop.admin.ui.publication.PostActionsActivity;

public class MainViewPagerFragment extends ViewPagerFragment<BaseFragment> {

    private TabLayout tabLayout;
    private FloatingActionButton newPostButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabLayout = new TabLayout(getContext());
        tabLayout.setSelectedTabIndicatorColor(getContext().getResources().getColor(R.color.darkish_green));
        tabLayout.setTabTextColors(getContext().getResources().getColor(R.color.greyish), getContext().getResources().getColor(R.color.darkish_green));

        newPostButton = new FloatingActionButton(getContext());
        newPostButton.setUseCompatPadding(true);
        newPostButton.setBackgroundTintList(ColorStateList.valueOf(getContext().getResources().getColor(R.color.darkish_green)));
        newPostButton.setCompatElevation(12);
        newPostButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));

        newPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), PostActionsActivity.class);
                startActivity(i);
            }
        });
        newPostButton.hide();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (toolbar != null) {
            toolbar.setNavigationIcon(null);
        }

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
        root.addView(newPostButton);
        int fabMargin = (int) getContext().getResources().getDimension(R.dimen.fab_margin);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) newPostButton.getLayoutParams();
        params.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        params.setMargins(0, 0, fabMargin, fabMargin);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    newPostButton.show();
                } else {
                    newPostButton.hide();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (getCurrentPosition() == 1) {
                        setCurrentPosition(0, true);
                        return true;
                    }
                    return false;
                }
                return false;
            }
        });

        return view;
    }

    protected void fragmentCreated(int position) {

    }

    @Override
    protected BaseFragment instantiateFragment(int position) {
        switch (position) {
            case 0:
                final SuggestionsFragment suggestionsFragment = new SuggestionsFragment();
                suggestionsFragment.getUpdatableModel().addListener(new UpdatableModel.UpdateListener<UpdatableModel>() {
                    @Override
                    public void updateStarted(UpdatableModel updatableModel) {

                    }

                    @Override
                    public void onModelUpdated(UpdatableModel updatableModel, boolean success, com.freeshop.admin.api.Error error) {
                        if (updatableModel instanceof UpdatableVkPostList) {
                            UpdatableVkPostList list = (UpdatableVkPostList) updatableModel;
                            if (getActivity() != null) {
                                setTabTitle(0, getResources().getString(R.string.suggestions_title) + " (" + list.getItems().size() + ")");
                            }
                        }
                    }
                });
                return suggestionsFragment;
            case 1:
                final NewsFeedFragment fragment = new NewsFeedFragment();
                fragment.getUpdatableModel().addListener(new UpdatableModel.UpdateListener<UpdatableModel>() {
                    @Override
                    public void updateStarted(UpdatableModel updatableModel) {

                    }

                    @Override
                    public void onModelUpdated(UpdatableModel updatableModel, boolean success, com.freeshop.admin.api.Error error) {
                        if (updatableModel instanceof UpdatableVkPostList) {
                            UpdatableVkPostList list = (UpdatableVkPostList) updatableModel;
                            if (getActivity() != null) {
                                setTabTitle(1, getResources().getString(R.string.news_title) + " (" + list.getTodayPostsCount() + ")");
                            }
                        }
                    }
                });
                fragment.setOnFragmentResumeListener(new NewsFeedFragment.OnFragmentResumeListener() {
                    @Override
                    public void onFragmentResumed() {
                        fragment.addOnScrollListener(scrollListener);
                    }
                });
                return fragment;
        }
        return null;
    }

    private void setTabTitle(int position, String title) {
        getTabLayout().getTabAt(position).setText(title);
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy < -60) {
                newPostButton.show();
            } else if (dy > 0) {
                newPostButton.hide();
            }
        }
    };

    @Override
    protected String getTabTitle(int position) {
        switch (position) {
            case 0:
                return getContext().getString(R.string.suggestions_title);
            case 1:
                return getContext().getString(R.string.news_title);
        }
        return "";
    }


    @Override
    protected int getCount() {
        return 2;
    }

    @Override
    protected TabLayout getTabLayout() {
        return tabLayout;
    }

    @Override
    public String getTitle() {
        return getAppContext().getString(R.string.main_title);
    }

}
