package com.freeshop.admin;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.vk.sdk.VKSdk;

import org.joda.time.DateTime;

import io.fabric.sdk.android.Fabric;

public class FreeshopAdminApplication extends Application {

    private static Context context;

    private static FreeshopAdminApplication instance;

    public static FreeshopAdminApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        FreeshopAdminApplication.context = getApplicationContext();

        VKSdk.initialize(getApplicationContext());
    }

    public static Context getAppContext() {
        return FreeshopAdminApplication.context;
    }
}
