package com.freeshop.admin.telegram;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.freeshop.admin.R;
import com.freeshop.admin.model.telegram.Channel;
import com.freeshop.admin.model.telegram.ChannelsList;

import java.util.ArrayList;

public class ChannelPickerDialog extends AlertDialog {

    private ListView listView;
    private Button okButton;
    private Button cancelButton;
    private OnChannelSelectedListener listener;
    private ChannelAdapter adapter;
    private CheckBox noNotificatonCheckbox;
    private boolean showCancel;

    public ChannelPickerDialog(Context context, boolean showCancel, OnChannelSelectedListener listener) {
        super(context);
        this.listener = listener;
        this.showCancel = showCancel;
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    public ChannelPickerDialog(Context context, OnChannelSelectedListener listener) {
        this(context, true, listener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channel_picker_dialog);

        listView = (ListView) findViewById(R.id.list_view);
        okButton = (Button) findViewById(R.id.ok);
        cancelButton = (Button) findViewById(R.id.cancel);
        noNotificatonCheckbox = (CheckBox) findViewById(R.id.sendSilentlyCheckbox);

        if (!showCancel) {
            cancelButton.setVisibility(View.GONE);
        }

        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        adapter = new ChannelAdapter(getContext(), ChannelsList.getList());
        listView.setAdapter(adapter);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Channel selectedItem = adapter.getSelectedItem();
                if (listener != null && selectedItem != null) {
                    listener.onChannelSelected(selectedItem, noNotificatonCheckbox.isChecked());
                }
                cancel();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setSelectedItemPosition(position);
                listView.setItemChecked(position, true);
            }
        });

        adapter.setSelectedItemPosition(0);
        listView.setItemChecked(0, true);
    }

    public interface OnChannelSelectedListener {
        void onChannelSelected(Channel item, boolean silently);
    }

    public static class ChannelAdapter extends BaseAdapter {
        private Context context;
        private LayoutInflater inflater;
        private ArrayList<Channel> channelArrayList;
        private int selectedItemPosition = -1;

        ChannelAdapter(Context context, ArrayList<Channel> channelArrayList) {
            this.context = context;
            this.channelArrayList = channelArrayList;
            inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return channelArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return channelArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                view = inflater.inflate(R.layout.channel_picker_row, parent, false);
            }

            Channel channel = channelArrayList.get(position);
            ((TextView) view.findViewById(R.id.text1)).setText(channel.getScreenName());

            return view;
        }

        public Channel getSelectedItem() {
            if (selectedItemPosition != -1) {
                return channelArrayList.get(selectedItemPosition);
            }
            return null;
        }

        public void setSelectedItemPosition(int selectedItemPosition) {
            this.selectedItemPosition = selectedItemPosition;
        }
    }
}

