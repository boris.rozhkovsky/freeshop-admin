package com.freeshop.admin;

public class Config {

    public static FreeshopGroup currentGroup;

    static {
        if (BuildConfig.BUILD_FLAVOUR.equalsIgnoreCase("prod")) {
            currentGroup = FreeshopGroup.FREESHOP_RENT;
        } else {
            currentGroup = FreeshopGroup.FRESSHOP_SHAHTY;
        }
    }

    public enum FreeshopGroup {
        FREESHOP_RENT(-58273617),
        FREESHOP_MAIN(-47670295),
        FRESSHOP_SHAHTY(-50375353);

        private long groupId;
        private String groupAccessToken;

        FreeshopGroup(long groupId) {
            this.groupId = groupId;
        }

        public long getGroupId() {
            return groupId;
        }
    }

}

